from rootpy import log
from rootpy.io import root_open

from .utils import *
from . import DrawBox

__all__ = [
        'LatexBox',
        ]

#Read config
cfg = load_cfg()

class LatexBox(object):
    def __init__(self, oniadata):
        self.data = oniadata
        self.figures = DrawBox(self.data)

        #Produce figures
        self.fig_pt = self.figures.draw_pt()

        self.figures.draw_y()
        self.fig_y = self.figures.fig_y

        self.figures.draw_maps()
        self.maps = self.figures.maps

        try:
            self.figures.draw_fitsys()
            self.fig_var_pt = self.figures.fig_var_pt

            self.fig_sys_pt = self.figures.draw_pt(sys=True)
        except KeyError:
            pass

        try:
            self.figures.draw_errors()
            self.fig_errors = self.figures.fig_errors
        except KeyError:
            pass

        try:
            self.figures.fitresults()
            self.fitted = self.figures.fitted
        except AttributeError:
            pass

    def table_chi(self):
        graph_store = self.data.graphs_pt
        graphs_pt = graph_store.store

        #FIXME
        f_w = root_open('/afs/cern.ch/work/b/bchargei/private/JPsi/JpsiAnalysis/2D/PostProcess/All/weights.root')

        fname = '{0}{1}_chi2.tex'.format(cfg['path_tables'], self.data.year)
        f_tex = open(fname, 'w')

        title = 'Two dimensional $\chi^2$ / ndof'

        graphs = graphs_pt['Chi2']
        tex = '\\begin{table}[h]\n'
        tex += '\\begin{center}\n'
        tex += '\\begin{tabular}{|c|c|c|c|}\n'
        tex += '\\hline\n'
        tex += '&'
        for iy in range(len(cfg['bins_y'])-1):
            tex += '{0:.2f} <|y|< {1:.2f} & '.format(cfg['bins_y'][iy], cfg['bins_y'][iy+1])
        tex = tex[:-2]
        tex += '\\\\ \n\\hline\n'
        for ipt in range(cfg['bins_pt'].index(cfg['min_pt']),len(cfg['bins_pt'])-1):
            pt_lo = cfg['bins_pt'][ipt]
            pt_hi = cfg['bins_pt'][ipt+1]
            tex += '{0} - {1} [GeV] & '.format(pt_lo, pt_hi)
            for g in graphs:
                iy = get_iy(g.name)
                chi2 = g[ipt].y.value
                ndof = g[ipt].y.error_hi

                h_w = f_w['weight_tot_y{0}_pt{1}'.format(iy, ipt)]
                ndof *= 1+(h_w.get_rms()/h_w.get_mean())**2

                tex += '{0:.0f} / {1:.0f} & '.format(chi2, ndof)
            tex = tex[:-2]
            tex += '\\\\ \n'
        tex += '\\hline\n'
        tex += '\\end{tabular}\n'
        tex += '\\caption{{{0}}}\n'.format(title)
        tex += '\\end{center}\n'
        tex += '\\end{table}\n'
        f_tex.write(tex)
        f_tex.close()

    def table_sys(self):
        graph_store = self.data.graphs_pt.clone()
        graphs_stat = graph_store.store
        graphs_sys = self.data.sys_graphs_pt.clone().store
        fname = '{0}{1}.tex'.format(cfg['path_tables'], self.data.year)
        f_tex = open(fname, 'w')
        tex = ''
        for par_name in cfg['par_extra']:
            title = cfg['par_extra'][par_name][0]
            text  = cfg['par_extra'][par_name][1]

            title = title.replace("J/#psi", "\jpsi")
            title = title.replace("#psi(2S)", "\psip")
            title = title.replace("#tau", "$\\tau$")
            title = title.replace("#chi^{2}/ndof", "$\chi^{2}/ndof$")


            pairs = [[g_stat.Crop(cfg['min_pt'],cfg['bins_pt'][-1]), g_sys] for g_stat, g_sys in zip(graphs_stat[par_name], graphs_sys[par_name])]
            tex += '\\begin{table}[h]\n'
            tex += '\\begin{center}\n'
            for p in pairs:
                iy = get_iy(p[0].name)

                tex += '\\begin{tabular}{|c|c|c|c|}\n'
                tex += '\\hline\n'
                tex += '&\\multicolumn{{3}}{{c|}}{{{0:.2f} <|y|< {1:.2f}}} \\\\ \n'.format(cfg['bins_y'][iy], cfg['bins_y'][iy+1])
                tex += '\\hline\n'
                #tex += '$p_{{T}}$ & {0} & Stat. uncertainty \% & Fit model uncertainty \% \\\\ \n'.format(title)
                tex += '$p_{T}$ & Value & Stat. uncertainty \% & Fit model uncertainty \% \\\\ \n'
                tex += '\\hline\n'
                for p_stat, p_sys in zip(*p):
                    ipt = p_stat.idx_ + 23
                    pt_lo = cfg['bins_pt'][ipt]
                    pt_hi = cfg['bins_pt'][ipt+1]
                    val = p_stat.y.value
                    err_stat = p_stat.y.error_hi/val
                    err_sys_hi = p_sys.y.error_hi/val
                    err_sys_low = p_sys.y.error_low/val

                    val, pval = scientific(val)
                    err_stat, perr_stat = scientific(err_stat)
                    err_sys_hi, perr_sys_hi = scientific(err_sys_hi)
                    err_sys_low, perr_sys_low = scientific(err_sys_low)

                    tex += '{0} - {1} [GeV]& ${2:.2f} \\times 10^{{{3:.1g}}}$ & ${4:.2f} \\times 10^{{{5:.1g}}}$ & ${6:.2f} \\times 10^{{{7:.1g}}}$, ${8:.2f} \\times 10^{{{9:.1g}}}$ \\\\ \n'.format(
                            pt_lo, pt_hi, val,pval, err_stat,perr_stat, err_sys_hi,perr_sys_hi, err_sys_low, perr_sys_low)
                tex += '\\hline\n'
                tex += '\\end{tabular}\n'

            tex += '\\caption{{{0}}}\n'.format(title)
            tex += '\\end{center}\n'
            tex += '\\end{table}\n'
        f_tex.write(tex)
        f_tex.close()




    def par_vs_x(self, par_name, x='pt'):
        """
        Draw parameter evolution vs x figures and produce latex code
        x: str object, either 'pt' or 'y'
        par_name: str object, parameter name

        Returns:
        str, latex code for given par_name vs x
        """
        if x == 'pt':
            fignames = self.fig_pt
        elif x == 'y':
            fignames = self.fig_y
        else:
            raise ValueError("x should be either 'pt' or 'y'")

        try:
            tex = '\\begin{figure}[h]\n'
            tex += '\\centering\n'
            tex += '\\includegraphics[width=.9\columnwidth]{{{0}}}\n'.format(fignames[par_name])

            title = cfg['par_table'][par_name]['y_title']
            mult  = cfg['par_table'][par_name]['scale']
            #xsection titles
            if par_name in cfg['par_extra']:
                title = cfg['par_extra'][par_name][0]
                text  = cfg['par_extra'][par_name][0]

            title = title.replace("J/#psi", "\jpsi")
            title = title.replace("#psi(2S)", "\psip")
            title = title.replace("#tau", "$\\tau$")
            title = title.replace("#chi^{2}/ndof", "$\chi^{2}/ndof$")
            title = title.lower()

            if x == 'pt':
                capt = ("The {0} as a function of \pt($\mu\mu$) for each of the "
                        "slices of rapidity. For each increasing rapidity "
                        "slice, a scaling factor of x 10 is applied to the "
                        "plotted points. The centre of each bin on the "
                        "horizontal axis represents the mean of the weighted "
                        "\pt distribution. The horizontal error-bars represent "
                        "the range of \pt for the bin, and the vertical "
                        "error-bar covers the statistical uncertainty "
                        "(with the same multiplicative scaling applied) only.").format(title)
                if not mult:
                    capt = ("The {0} as a function of \pt($\mu\mu$) for each of"
                            "the slices of rapidity. The centre of each bin on "
                            "the horizontal axis represents the mean of the "
                            "weighted \pt distribution. The horizontal "
                            "error-bars represent the range of \pt for the bin, "
                            "and the vertical error-bar covers the statistical "
                            "uncertainty only.").format(title)
            elif x == 'y':
                capt = ("The {0} as a function of |y($\mu\mu$)| for each of the "
                        "slices of \pt. The centre of each bin on the horizontal"
                        "axis represents the mean of the weighted |y| distribution."
                        "The horizontal error-bars represent the range of |y| for "
                        "the bin, and the vertical error-bar covers the "
                        "statistical uncertainty only.").format(title)

            tex += '\\caption{{{0}}}\n'.format(capt)
            tex += '\\end{figure}\n'
        except KeyError:
            log.warning("{} not found in parameter collection".format(par_name))
            tex=''

        return tex

    def par_2D(self, par_name):
        """
        Draw parameter 2D maps and produce latex code
        par_name: str object, parameter name

        Returns:
        str, latex code for given par_name 2D maps
        """
        try:
            tex = '\\begin{figure}[h]\n'
            #tex += '\\centering\n'
            tex += '\\subfigure[]{{\includegraphics[width=.46\columnwidth]{{{0}}}}}\n'.format(self.maps[par_name][0])
            tex += '\\subfigure[]{{\includegraphics[width=.46\columnwidth]{{{0}}}}}\n'.format(self.maps[par_name][1])

            title = cfg['par_table'][par_name]['y_title']
            #xsection titles
            if par_name in cfg['par_extra']:
                title = cfg['par_extra'][par_name][0]
                text  = cfg['par_extra'][par_name][1]

            title = title.replace("J/#psi", "\jpsi")
            title = title.replace("#psi(2S)", "\psip")
            title = title.replace("#tau", "$\\tau$")
            title = title.replace("#chi^{2}/ndof", "$\chi^{2}/ndof$")
            title = title.lower()

            capt = "The {0}. (a) Value of the fit (b) Statistical uncertainty".format(title)
            tex += '\\caption{{{0}}}\n'.format(capt)
            tex += '\\end{figure}\n'
        except KeyError:
            log.warning("{} not found in parameter collection".format(par_name))
            tex=''

        return tex

    def par_maps(self):
        fname = '{0}{1}_par_maps.tex'.format(cfg['path_latex'], self.data.year)
        tex = open(fname, 'w')
        for i, par_name in enumerate(cfg['par_table']):
            #2D maps
            tex.write(self.par_2D(par_name))
            #if not (i+1)%3:
                #tex.write('\\clearpage\n')
        tex.close()

    def par_pt_y(self):
        """
        Draw main parameter evolution vs pt and y

        Output latex code will be written at path_latex/par_pt_y.tex file
        """

        fname = '{0}{1}_par_pt_y.tex'.format(cfg['path_latex'], self.data.year)
        tex = open(fname, 'w')

        for par_name in cfg['par_extra']:
            #pt-dependent figures
            tex.write(self.par_vs_x(par_name, 'pt'))

            #y-dependent figures
            tex.write(self.par_vs_x(par_name, 'y'))
            tex.write('\\clearpage\n')

        tex.close()

    def par_map_pt_y(self):
        """
        Draw parameter evolution vs pt and y, obtain 2D maps
        and produce latex code

        Output latex code will be written at path_latex/par_map_pt_y.tex file
        """

        fname = '{0}{1}_par_map_pt_y.tex'.format(cfg['path_latex'], self.data.year)
        tex = open(fname, 'w')

        for par_name in cfg['par_table']:
            #2D maps
            tex.write(self.par_2D(par_name))

            #pt-dependent figures
            tex.write(self.par_vs_x(par_name, 'pt'))

            #y-dependent figures
            tex.write(self.par_vs_x(par_name, 'y'))
            tex.write('\\clearpage\n')

        tex.close()

    def fit_pt_sys(self):
        fname = '{0}{1}_fit_pt_sys.tex'.format(cfg['path_latex'], self.data.year)
        f_tex = open(fname, 'w')
        for par_name in cfg['par_extra']:
            tex = '\\begin{figure}[h]\n'
            #tex += '\\centering\n'
            fig = self.fig_sys_pt[par_name]
            tex += '\\subfigure[]{{\includegraphics[width=.9\columnwidth]{{{0}}}}}\n'.format(fig)

            title = cfg['par_extra'][par_name][0]
            text  = cfg['par_extra'][par_name][1]

            title = title.replace("J/#psi", "\jpsi")
            title = title.replace("#psi(2S)", "\psip")
            title = title.replace("#tau", "$\\tau$")
            title = title.replace("#chi^{2}/ndof", "$\chi^{2}/ndof$")
            title = title.lower()

            capt = ("The {0} as a function of \pt($\mu\mu$) for each of the "
                    "slices of rapidity. For each increasing rapidity "
                    "slice, a scaling factor of x 10 is applied to the "
                    "plotted points. The centre of each bin on the "
                    "horizontal axis represents the mean of the weighted "
                    "\pt distribution. The horizontal error-bars represent "
                    "the range of \pt for the bin, and the vertical "
                    "error-bar covers the statistical uncertainty. "
                    "Uncertainties comming from fit model variation are marked "
                    "with filled area.").format(title)

            tex += '\\caption{{{0}}}\n'.format(capt)
            tex += '\\end{figure}\n'

            f_tex.write(tex)
        f_tex.close()

    def fit_pt_variation(self):
        fname = '{0}{1}_fit_pt_variation.tex'.format(cfg['path_latex'], self.data.year)
        f_tex = open(fname, 'w')
        for par_name in cfg['par_extra']:
            tex = '\\begin{figure}[h]\n'
            #tex += '\\centering\n'
            for fig in self.fig_var_pt[par_name]:
                tex += '\\subfigure[]{{\includegraphics[width=.9\columnwidth]{{{0}}}}}\n'.format(fig)

            title = cfg['par_extra'][par_name][0]
            text  = cfg['par_extra'][par_name][1]

            title = title.replace("J/#psi", "\jpsi")
            title = title.replace("#psi(2S)", "\psip")
            title = title.replace("#tau", "$\\tau$")
            title = title.replace("#chi^{2}/ndof", "$\chi^{2}/ndof$")
            title = title.lower()

            capt = "The {0} in rapidity slices".format(title)
            tex += '\\caption{{{0}}}\n'.format(capt)
            tex += '\\end{figure}\n'

            f_tex.write(tex)
        f_tex.close()

    def error_hists(self):
        fname = '{0}{1}_error_hists.tex'.format(cfg['path_latex'], self.data.year)
        f_tex = open(fname, 'w')
        for par_name in cfg['par_extra']:
            tex = '\\begin{figure}[h]\n'
            #tex += '\\centering\n'
            for fig in self.fig_errors[par_name]:
                tex += '\\subfigure[]{{\includegraphics[width=.33\columnwidth]{{{0}}}}}\n'.format(fig)

            title = cfg['par_extra'][par_name][0]
            text  = cfg['par_extra'][par_name][1]

            title = title.replace("J/#psi", "\jpsi")
            title = title.replace("#psi(2S)", "\psip")
            title = title.replace("#tau", "$\\tau$")
            title = title.replace("#chi^{2}/ndof", "$\chi^{2}/ndof$")
            title = title.lower()

            capt = ("The fractional uncertainty contributions of the {0} "
                    "shown as a function of $p_T$ in bins of increasing "
                    "rapidity.").format(title)
            tex += '\\caption{{{0}}}\n'.format(capt)
            tex += '\\end{figure}\n'

            f_tex.write(tex)
        f_tex.close()

    def year_graph(self):
        """
        Draw par vs year graphs and produce latex code
        """
        figs = DrawBox(self.data).draw_year()
        fname = '{0}{1}_bins.tex'.format(cfg['path_latex'], self.data.year)
        f_tex = open(fname, 'w')
        tex = ''

        for par_name in cfg['par_extra']:
            par_figs = figs[par_name]
            tex+= '\\begin{figure}\n'
            for fig in par_figs:
                tex +=('\\subfigure[]{{\includegraphics[width=.1\columnwidth]{{{0}}}}}\n'.format(fig))
            tex+= '\\end{figure}\n'

        f_tex.write(tex)
        f_tex.close()



    def fitresults(self):
        """
        Draw fitted histograms and produce the corrseponding latex code

        Output latex code will be written at path_latex/fitresults.tex file
        """
        fname = '{0}{1}_fitresults.tex'.format(cfg['path_latex'], self.data.year)
        tex = open(fname, 'w')
        for i, (fig_m, fig_tau) in enumerate(self.fitted):
            if not i%3:
                tex.write('\\begin{figure}[h]\n')
                tex.write('\\centering\n')
            tex.write('\\subfigure[]{{\includegraphics[width=.4\columnwidth]{{{0}}}}}\n'.format(fig_m))
            tex.write('\\subfigure[]{{\includegraphics[width=.4\columnwidth]{{{0}}}}}\\\\\n'.format(fig_tau))

            if not (i+1)%3:
                capt = ("Fit results can be seen from the projection of the "
                        "likelihood on the $m$ and $\\tau$ dimensions. "
                        "Corresponding \pt and absolute rapidity ranges are "
                        "specified on the plot. The projection of the data "
                        "and fit results for invariant mass is shown in the "
                        "left column, and the projection as a function of "
                        "pseudo-proper lifetime is shown in the right column")

                tex.write('\\caption{{{0}}}\n'.format(capt))
                tex.write('\\end{figure}\n')
                tex.write('\\clearpage\n')
