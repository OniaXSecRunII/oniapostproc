from __future__ import absolute_import

import random
from math import pi
from tqdm import tqdm
from rootpy.io import root_open, Directory
from rootpy import asrootpy, log
from rootpy.plotting import Hist, Graph, Canvas
from collections import OrderedDict

from .utils import *

__all__ = [
        'SysTools',
        ]

#Read config
cfg = load_cfg()

class SysTools(object):
    def __init__(self, year, ntuples):
        self.year = year
        self.trees = []
        self.rand_maps_trigger = []
        self.rand_maps_reco = []
        self.trigger_histos = OrderedDict()
        self.reco_histos = OrderedDict()
        self.trigger_means = OrderedDict()
        self.reco_means = OrderedDict()

        self.ntuples = ntuples

        self.gen_randmaps_trigger()
        self.gen_randmaps_reco()
        self.apply_variation()

        with root_open(cfg['path_rootfiles']+'reco_varied.root','w') as tf:
            for iy, ipt in self.reco_histos:
                h_out = Hist(50,1,2,name='h_y{0}_pt{1}'.format(iy, ipt))
                d = Directory('y{0}_pt{1}'.format(iy, ipt))
                #Loop in varied weights
                d.cd()
                for h in self.reco_histos[iy, ipt]:
                    h.write()
                    h_out.fill(h.get_mean())
                    print(h.get_mean())
                d.close()
                h_out.write()
                try:
                    self.reco_means[iy][ipt] = (ipt, h_out.get_rms()/h_out.get_mean())
                except KeyError:
                    self.reco_means[iy] = Graph(len(cfg['bins_pt'])-1, name='g_reco_y{0}'.format(iy))
                    self.reco_means[iy][ipt] = (ipt, h_out.get_rms()/h_out.get_mean())

        with root_open(cfg['path_rootfiles']+'trigger_varied.root','w') as tf:
            for iy, ipt in self.trigger_histos:
                h_out = Hist(50,1,2,name='h_y{0}_pt{1}'.format(iy, ipt))
                d = Directory('y{0}_pt{1}'.format(iy, ipt))
                d.cd()
                for h in self.trigger_histos[iy, ipt]:
                    h.write()
                    h_out.fill(h.get_mean())
                d.close()
                h_out.write()
                try:
                    self.trigger_means[iy][ipt] = (ipt, h_out.get_rms()/h_out.get_mean())
                except KeyError:
                    self.trigger_means[iy] = Graph(len(cfg['bins_pt'])-1, name='g_trigger_y{0}'.format(iy))
                    self.trigger_means[iy][ipt] = (ipt, h_out.get_rms()/h_out.get_mean())

        with root_open(cfg['path_rootfiles']+'sys.root','w') as tf:
            for iy in self.reco_means:
                self.reco_means[iy].write()
            for iy in self.trigger_means:
                self.trigger_means[iy].write()

        #h_out = Hist(40,1,2)
        #for h in self.w_histos:
            #h_out.fill(h.get_mean())
        #h_out.save_as('/tmp/bchargei/sys.root')

        with root_open(cfg['path_rootfiles']+'reco_rand_maps.root','w') as tf:
            for m1, m2 in self.rand_maps_reco:
                c = Canvas(name=m1.name, width=800, height=600)
                c.draw()
                m1.draw('colztext')
                c.write()

                c = Canvas(name=m2.name, width=800, height=600)
                m2.draw('colztext')
                c.write()

        with root_open(cfg['path_rootfiles']+'trigger_rand_maps.root','w') as tf:
            for m1, m2 in self.rand_maps_trigger:
                c = Canvas(name=m1.name, width=800, height=600)
                c.draw()
                m1.draw('colztext')
                c.write()

                c = Canvas(name=m2.name, width=800, height=600)
                m2.draw('colztext')
                c.write()

    @property
    def year(self):
        return self.__year

    @year.setter
    def year(self, year):
        if not isinstance(year, int):
            raise TypeError("year should be of type int")
        else:
            self.__year = year

    def gen_randmaps_trigger(self):
        with root_open(cfg['trig_eff_maps'][self.year]) as file_eff:
            period = list(file_eff.walk())[1][1][0] #some existing period
            for i in range(cfg['var_number']):
                log.info("Generating trigger variation map #{0}...".format(i))
                h_barrel=file_eff['Tight/{0}/HLT_mu50/eff_etaphi_fine_barrel_data_nominal'.format(period)].clone()
                h_endcap=file_eff['Tight/{0}/HLT_mu50/eff_etaphi_fine_endcap_data_nominal'.format(period)].clone()

                h_barrel.set_directory(0)
                h_endcap.set_directory(0)

                h_barrel.name = 'barrel_var{0}'.format(i)
                h_endcap.name = 'endcap_var{0}'.format(i)

                for ibin in h_barrel:
                    if ibin.value == 0:
                        continue
                    ibin.value = random.gauss(0, i!=0)
                    #if i == 0:
                        #ibin.value = 0
                    #else:
                        #ibin.value = 1

                for ibin in h_endcap:
                    if ibin.value == 0:
                        continue
                    ibin.value = random.gauss(0, i!=0)
                    #if i == 0:
                        #ibin.value = 0
                    #else:
                        #ibin.value = 1

                self.rand_maps_trigger.append((h_barrel,h_endcap))

    def gen_randmaps_reco(self):
        with root_open(cfg['reco_eff_maps'][0]) as f_jpsi, root_open(cfg['reco_eff_maps'][1]) as f_z:
            for i in range(cfg['var_number']):
                log.info("Generating reco variation map #{0}...".format(i))
                h_jpsi=f_jpsi['Eff_{0}'.format(self.year)].clone()
                h_z=f_z['Eff_{0}'.format(self.year)].clone()

                h_jpsi.set_directory(0)
                h_z.set_directory(0)

                h_jpsi.name = 'jpsi_var{0}'.format(i)
                h_z.name = 'z_var{0}'.format(i)

                for ibin in h_jpsi:
                    if ibin.value == 0:
                        continue
                    ibin.value = random.gauss(0, i!=0)

                for ibin in h_z:
                    if ibin.value == 0:
                        continue
                    ibin.value = random.gauss(0, i!=0)

                #Create empty histos for weights

                self.rand_maps_reco.append((h_jpsi,h_z))

    def apply_variation(self):
        for iy, (y_lo, y_hi) in enumerate(zip(cfg['bins_y'], cfg['bins_y'][1:])):
            for ipt, (pt_lo, pt_hi) in enumerate(zip(cfg['bins_pt'], cfg['bins_pt'][1:])):
                if pt_lo < cfg['min_pt']:
                    continue
                for i, ntuple in enumerate(self.ntuples):
                    log.info('Processing file {0}, bin y {1} pt {2}'.format(ntuple,iy,ipt))
                    with root_open(ntuple) as f:
                        tree = f['tuple_y{0}_pt{1}'.format(iy, ipt)]
                        #Create empty histos for varied weights
                        for ivar in range(cfg['var_number']):
                            h = Hist(80,1,9,name='w_trigger_y{0}_pt{1}_var{2}'.format(iy,ipt,ivar))
                            h.set_directory(0)

                            if ivar == 0:
                                self.trigger_histos[iy,ipt] = []
                                self.reco_histos[iy,ipt] = []

                            self.trigger_histos[iy,ipt].append(h)

                            h = Hist(80,1,9,name='w_reco_y{0}_pt{1}_var{2}'.format(iy,ipt,ivar))
                            h.set_directory(0)
                            self.reco_histos[iy,ipt].append(h)

                        num_entries = tree.get_entries_fast()
                        pbar = tqdm(total=num_entries)
                        for iev,ev in enumerate(tree):
                            pbar.update()
                            if not (ev.passTrigger & cfg['trigger_cut'][self.year][i]):
                                continue
                            if ev.weight_reco >= 20 or not ev.weight_trig_single:
                                continue
                            if ev.pt1<52.5 and ev.pt2<52.5:
                                continue

                            eff_trig_mu0_up   = ev.eff_trig_single_mu0_up
                            eff_trig_mu1_up   = ev.eff_trig_single_mu1_up
                            eff_trig_mu0_down = ev.eff_trig_single_mu0_down
                            eff_trig_mu1_down = ev.eff_trig_single_mu1_down
                            eff_trig_mu0_nom  = ev.eff_trig_single_mu0
                            eff_trig_mu1_nom  = ev.eff_trig_single_mu1

                            eff_reco_mu0_up   = ev.eff_reco_mu0_up #up/down are already deltas
                            eff_reco_mu1_up   = ev.eff_reco_mu1_up
                            eff_reco_mu0_down = ev.eff_reco_mu0_down
                            eff_reco_mu1_down = ev.eff_reco_mu1_down
                            eff_reco_mu0_nom  = ev.eff_reco_mu0
                            eff_reco_mu1_nom  = ev.eff_reco_mu1

                            mu0_eta = ev.qeta1/ev.q1
                            mu1_eta = ev.qeta2/ev.q2
                            mu0_phi = ev.qphi1/ev.q1
                            mu1_phi = ev.qphi2/ev.q2

                            mu0_pt = ev.pt1
                            mu1_pt = ev.pt2

                            for ivar in range(cfg['var_number']):
                                #Trigger
                                var_mu0 = self.get_variation(ivar, 'trigger', mu0_eta, mu0_phi)
                                var_mu1 = self.get_variation(ivar, 'trigger', mu1_eta, mu1_phi)

                                e0 = eff_trig_mu0_nom + var_mu0 * (eff_trig_mu0_up - eff_trig_mu0_nom)
                                e1 = eff_trig_mu1_nom + var_mu1 * (eff_trig_mu1_up - eff_trig_mu1_nom)
                                w = 1/(1-(1-e0)*(1-e1))

                                self.trigger_histos[iy,ipt][ivar].fill(w)

                                #Reco
                                var_mu0 = self.get_variation(ivar, 'reco', mu0_eta, mu0_phi, mu0_pt)
                                var_mu1 = self.get_variation(ivar, 'reco', mu1_eta, mu1_phi, mu1_pt)

                                e0 = eff_reco_mu0_nom + var_mu0 * eff_reco_mu0_up
                                e1 = eff_reco_mu1_nom + var_mu1 * eff_reco_mu1_up
                                w = 1/(e0*e1)
                                #print(eff_reco_mu0_nom,eff_reco_mu0_up)

                                self.reco_histos[iy,ipt][ivar].fill(w)

                        pbar.close()

    def get_variation(self, ivar, sys, eta, phi, pt=None):
        if sys == 'trigger':
            is_barrel = abs(eta) < 1.05

            if is_barrel:
                h = self.rand_maps_trigger[ivar][0]
            else:
                h = self.rand_maps_trigger[ivar][1]

            if (phi < h.GetYaxis().GetXmin()):
                phi += 2.0 * pi
            if (phi > h.GetYaxis().GetXmax()):
                phi -= 2.0 * pi

            ibin = h.find_bin(eta, phi)

            return h[ibin].value
        elif sys == 'reco':
            is_low = pt < 15

            if is_low:
                h = self.rand_maps_reco[ivar][0]
                ibin = h.find_bin(eta, pt)

                return h[ibin].value
            else:
                h = self.rand_maps_reco[ivar][1]

                if (phi < h.GetYaxis().GetXmin()):
                    phi += 2.0 * pi
                if (phi > h.GetYaxis().GetXmax()):
                    phi -= 2.0 * pi

                ibin = h.find_bin(eta, phi)

                return h[ibin].value
