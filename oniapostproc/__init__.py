from __future__ import absolute_import

import sys
import os
import oyaml as yaml
import logging
logging.basicConfig(level=logging.WARNING)
import rootpy.ROOT as R
from rootpy.io import root_open
from rootpy.plotting import Hist, Hist2D, Canvas
from rootpy import log

from .oniadata import OniaData, OniaCombData
from .drawbox import DrawBox
from .latexbox import LatexBox
from .systools import SysTools
from .utils import *

__all__ = [
        'init_workdir',
        'OniaData',
        'OniaCombData',
        'DrawBox',
        'LatexBox',
        'SysTools',
        ]

p_dir = os.path.dirname(os.path.abspath(__file__))

R.gROOT.SetBatch(1)
R.gROOT.LoadMacro(os.path.join(p_dir, "../plotstyle/AtlasStyle.C"))
R.gROOT.LoadMacro(os.path.join(p_dir, "../plotstyle/AtlasUtils.C"))
R.gROOT.LoadMacro(os.path.join(p_dir, "../plotstyle/AtlasLabels.C"))
R.SetAtlasStyle()
R.TH1.SetDefaultSumw2(False)

#Read config
cfg = load_cfg()

def init_workdir():
    """
    Initialize workdir structure
    """
    #Create a folder for pt dependent graphs
    if not os.path.exists(cfg['path_pt_graphs']):
        os.makedirs(cfg['path_pt_graphs'])

    #Create a folder for error hists
    if not os.path.exists(cfg['path_errors']):
        os.makedirs(cfg['path_errors'])

    #Create a folder for y dependent graphs
    if not os.path.exists(cfg['path_y_graphs']):
        os.makedirs(cfg['path_y_graphs'])

    #Create a folder for 2D parameter maps
    if not os.path.exists(cfg['path_par_2D']):
        os.makedirs(cfg['path_par_2D'])

    #Create a folder for fitted plots
    if not os.path.exists(cfg['path_fitted']):
        os.makedirs(cfg['path_fitted'])

    #Create a folder for 2D weight maps
    if not os.path.exists(cfg['path_weight_2D']):
        os.makedirs(cfg['path_weight_2D'])

    #Create a folder for par vs year graphs
    if not os.path.exists(cfg['path_year_graphs']):
        os.makedirs(cfg['path_year_graphs'])

    #Create a folder for outpoot root files (Acceptance maps, etc...)
    if not os.path.exists(cfg['path_rootfiles']):
        os.makedirs(cfg['path_rootfiles'])

    #Create a folder for latex output
    if not os.path.exists(cfg['path_latex']):
        os.makedirs(cfg['path_latex'])

    #Create a folder for tables
    if not os.path.exists(cfg['path_tables']):
        os.makedirs(cfg['path_tables'])
