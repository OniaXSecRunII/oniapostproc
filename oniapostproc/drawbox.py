from __future__ import absolute_import

import re
import rootpy.ROOT as R
from rootpy.plotting import Graph, Hist, Canvas, Pad, base
from rootpy.plotting.utils import draw
from rootpy.io import root_open
from rootpy.plotting.style import get_style
from rootpy import log, asrootpy

from .utils import *

__all__ = [
        'DrawBox',
        ]

#Read config
cfg = load_cfg()

class DrawBox(object):
    def __init__(self, oniadata):
        self.data = oniadata
        self.fig_pt = {} #Path of pt-dependent figures
        self.fig_y = {} #Path of y-dependent figures
        self.maps = {} #Path of 2D parameter maps
        self.fitted = [] #Path of 2D parameter maps
        self.fig_var_pt = {} #Path of pt-dependent sys figures
        self.fig_errors = {} #Path of error hists

        self.errors = {} #Total error histograms

    def draw_fitsys(self):
        """
        Draw individual fit systemcatics
        """
        gstore = self.data.apply_fitsys()[1]
        graphs_varied = gstore.store

        colors = get_colors(len(self.data.graphs_pt.sys_set))

        for par_name in cfg['par_extra']:
            for iy in range(len(cfg['bins_y'])-1):
                plotables = []
                for isys in self.data.graphs_pt.sys_set:
                    #FIXME tmp solution for obsolete variation removal
                    if isys == 7:
                        continue

                    graph = gstore['y{0}_{1}_sys{2}'.format(iy, par_name, isys)]

                    is_jpsi = not '2S' in graph.name and not 'R_NP' in graph.name and not 'R_P' in graph.name
                    if not is_jpsi:
                        graph = graph.Crop(cfg['min_pt'], 140)
                    else:
                        graph = graph.Crop(cfg['min_pt'], cfg['bins_pt'][-1])

                    graph.drawstyle = 'p'
                    graph.color = colors(isys)
                    #graph.linecolor = 'white'
                    graph.linewidth = 0
                    markerstyle = 20+(isys-1)
                    if markerstyle >= 35:
                        markerstyle = 39+(isys-16)
                    graph.markerstyle = markerstyle
                    graph.markersize = 1.3
                    plotables.append(graph)

                #graph0 = plotables[0].clone()
                graph0 = Graph(1)
                graph0[0].y.value = 0
                graph0[0].x.value = cfg['min_pt']
                graph0[0].x.error_hi = cfg['bins_pt'][-1]

                plotables.insert(0,graph0)

                graph0.drawstyle = ''
                graph0.color = 'black'
                graph0.markersize = 0
                graph0.linewidth = 2
                graph0.linestyle = 'dashed'
                graph0.xaxis.title = "#it{p}_{T}(#mu#mu) [GeV]"
                graph0.yaxis.title = 'Fractional Uncertainty'
                graph0.yaxis.set_title_size(0.05)
                graph0.yaxis.set_title_offset(1.2)
                graph0.xaxis.set_more_log_labels(True)
                graph0.xaxis.set_no_exponent(True)

                c = Canvas(width=800, height=300)
                c.name='unc_{0}_y{1}'.format(par_name, iy)
                p0 = Pad(0, 0, 0.85, 1)
                p1 = Pad(0.82, 0, 1, 1)
                c.draw()
                p0.draw('same')
                p1.draw('same')
                p0.cd()

                draw(plotables, pad=p0, logx=True, ylimits=(-0.2,0.2))

                if cfg['prelim']:
                    R.ATLASLabel(0.18,0.88,"Preliminary", 1, 0.06, 0.09);
                else:
                    R.ATLASLabel(0.18,0.88,"Internal", 1, 0.06, 0.09);
                R.myText(0.18,0.83,1, "{0:.2f} < |#it{{y}}| < {1:.2f}".format(cfg['bins_y'][iy],cfg['bins_y'][iy+1]));
                R.myText(0.18,0.78,1,cfg['par_extra'][par_name][0]);

                p1.cd()
                for i, graph in enumerate(plotables):
                    color = base.convert_color(graph.markercolor, 'root')
                    marker = base.convert_markerstyle(graph.markerstyle, 'root')
                    R.myMarkerText(0.1,0.96-0.05*i,color,marker,cfg['sys_table'][i],1.0,0.08);

                #Save
                figname = '{0}{1}_{2}.pdf'.format(cfg['path_pt_graphs'],self.data.year,c.name)
                figname_root = '{0}{1}_{2}.root'.format(cfg['path_pt_graphs'],self.data.year,c.name)
                c.save_as(figname)
                c.save_as(figname_root)
                try:
                    self.fig_var_pt[par_name].append(figname[7:])
                except KeyError:
                    self.fig_var_pt[par_name] = []
                    self.fig_var_pt[par_name].append(figname[7:])

    def draw_errors(self):
        """
        Draw all errors on one plot, should be ran at least after fit model sys
        calculation
        """
        graphs_stat = self.data.graphs_pt
        graphs_fitsys = self.data.sys_graphs_pt
        f_bin = root_open(cfg['bmgr_corr'])
        f_sf = root_open(cfg['sf_corr'])
        f_clos = root_open(cfg['clos_corr'])

        for par_name in cfg['par_extra']:
            is_jpsi = not '2S' in par_name and not 'R_NP' in par_name and not 'R_P' in par_name
            for iy in range(len(cfg['bins_y'])-1):
                plotables = []
                g_stat = graphs_stat['y{0}_{1}'.format(iy, par_name)]
                g_fitsys = graphs_fitsys['y{0}_{1}'.format(iy, par_name)]
                g_bmgr = f_bin['bmgr_sys_y{0}'.format(iy)]
                h_sf_reco_sys = asrootpy(f_sf['sf_reco_up_l'].projection_y('rpy_sys',iy+1,iy+1))
                h_sf_reco_stat = asrootpy(f_sf['sf_reco'].projection_y('rpy_stat',iy+1,iy+1))
                h_sf_trig_sys = asrootpy(f_sf['sf_trig_up_l'].projection_y('tpy_sys',iy+1,iy+1))
                h_sf_trig_stat = asrootpy(f_sf['sf_trig'].projection_y('tpy_stat',iy+1,iy+1))
                h_clos_reco = asrootpy(f_clos['muonreco'].projection_y('rpy_clos',iy+1,iy+1))
                h_clos_trig = asrootpy(f_clos['triggerreco'].projection_y('tpy_clos',iy+1,iy+1))

                h = Hist(cfg['bins_pt'], name = "h_stat")
                for p in g_stat:
                    bin_no = h.FindBin(p.x.value)
                    if not is_jpsi and h[bin_no].x.low >= 140:
                        continue
                    if p.y.value == 0:
                        continue
                    h[bin_no].value = 1e2*(p.y.error_hi+p.y.error_low)/(2*p.y.value)
                    h[bin_no].error = 0
                h.fillstyle = 3004
                h.color = 8
                plotables.append(h)

                h = Hist(cfg['bins_pt'], name = "h_fitsys")
                for p in g_fitsys:
                    bin_no = h.FindBin(p.x.value)
                    if not is_jpsi and h[bin_no].x.low >= 140:
                        continue
                    if p.y.value == 0:
                        continue
                    h[bin_no].value = 1e2*(p.y.error_hi+p.y.error_low)/((12**0.5)*p.y.value)
                    h[bin_no].error = 0
                h.color = 2
                h.linestyle = 10
                plotables.append(h)

                if 'N_1S' in par_name or 'N_2S' in par_name:
                    h = Hist(cfg['bins_pt'], name = "h_bmgr")
                    for p in g_bmgr:
                        bin_no = h.FindBin(p.x.value)
                        if h[bin_no].x.low < cfg['min_pt']:
                            continue
                        if not is_jpsi and h[bin_no].x.low >= 140:
                            continue
                        h[bin_no].value = p.y.value*1e2
                        h[bin_no].error = 0
                    h.color = 4
                    h.linestyle = 8
                    plotables.append(h)

                    h = Hist(cfg['bins_pt'], name = "h_reco")
                    for b in h_sf_reco_sys[1:-1]:
                        bin_no = h.FindBin(b.x.center)
                        if h[bin_no].x.low < cfg['min_pt']:
                            continue
                        if not is_jpsi and h[bin_no].x.low >= 140:
                            continue
                        #h[bin_no].value = ((b.value**2+(h_sf_reco_stat[b.idx].error/h_sf_reco_stat[b.idx].value)**2)**0.5)*1e2
                        #h[bin_no].value = b.value*1e2
                        val = b.value**2
                        val += (h_clos_reco[b.idx].error/h_clos_reco[b.idx].value)**2
                        #TODO auto first and last bin determination
                        if b.idx == 1:
                            val+= (abs(h_clos_reco[b.idx+1].value-h_clos_reco[b.idx].value)/(2*h_clos_reco[b.idx].value))**2
                        elif b.idx == 12:
                            val+= (abs(h_clos_reco[b.idx-1].value-h_clos_reco[b.idx].value)/(2*h_clos_reco[b.idx].value))**2
                        else:
                            val += ((abs(h_clos_reco[b.idx+1].value-h_clos_reco[b.idx].value)+abs((h_clos_reco[b.idx-1].value-h_clos_reco[b.idx].value)))/(2*h_clos_reco[b.idx].value))**2
                        h[bin_no].value = (val**0.5)*1e2
                        h[bin_no].error = 0
                    h.color = R.kOrange+7
                    h.linestyle = 7
                    plotables.append(h)

                    h = Hist(cfg['bins_pt'], name = "h_trig")
                    for b in h_sf_trig_sys[1:-1]:
                        bin_no = h.FindBin(b.x.center)
                        if h[bin_no].x.low < cfg['min_pt']:
                            continue
                        if not is_jpsi and h[bin_no].x.low >= 140:
                            continue
                        #h[bin_no].value = ((b.value**2+(h_sf_trig_stat[b.idx].error/h_sf_trig_stat[b.idx].value)**2)**0.5)*1e2
                        #h[bin_no].value = b.value*1e2
                        val = b.value**2
                        val += (h_clos_trig[b.idx].error/h_clos_trig[b.idx].value)**2
                        #TODO auto first and last bin determination
                        if b.idx == 1:
                            val+= (abs(h_clos_trig[b.idx+1].value-h_clos_trig[b.idx].value)/(2*h_clos_trig[b.idx].value))**2
                        elif b.idx == 12:
                            val+= (abs(h_clos_trig[b.idx-1].value-h_clos_trig[b.idx].value)/(2*h_clos_trig[b.idx].value))**2
                        else:
                            val += ((abs(h_clos_trig[b.idx+1].value-h_clos_trig[b.idx].value)+abs((h_clos_trig[b.idx-1].value-h_clos_trig[b.idx].value)))/(2*h_clos_trig[b.idx].value))**2
                        h[bin_no].value = (val**0.5)*1e2
                        h[bin_no].error = 0
                    h.color = R.kRed+2
                    h.linestyle = 6
                    plotables.append(h)

                h = Hist(cfg['bins_pt'], name = "h_sys")
                for b in h[1:-1]:
                    s = 0
                    for histo in plotables[1:]:
                        s += histo[b.idx].value**2
                    b.value = s**0.5
                    b.error = 0
                h.color = 6
                h.linestyle = 2
                plotables.append(h)

                h = Hist(cfg['bins_pt'], name = "h_err_all_y{0}".format(iy))
                for b in h[1:-1]:
                    s = 0
                    for histo in plotables[:-1]:
                        s += histo[b.idx].value**2
                    b.value = s**0.5
                    b.error = 0
                h.color = 1
                h.linestyle = 1
                plotables.append(h)
                h.set_directory(0)
                try:
                    self.errors[par_name].append(h)
                except KeyError:
                    self.errors[par_name] = []
                    self.errors[par_name].append(h)

                for h in plotables:
                    h.drawstyle = 'h'
                    h.markersize = 0
                    h.linewidth = 2

                c = Canvas(width=600, height=700)
                c.name='err_{0}_y{1}'.format(par_name, iy)

                #h0 = plotables[0].clone()
                #plotables.insert(0,h0)

                plotables[0].xaxis.title = "#it{p}_{T}(#mu#mu) [GeV]"
                plotables[0].yaxis.title = "Fractional Uncertainty [%]"
                plotables[0].yaxis.set_title_size(0.04)
                plotables[0].yaxis.set_title_offset(1.55)
                plotables[0].xaxis.set_more_log_labels(True)
                plotables[0].xaxis.set_no_exponent(True)

                print(plotables)
                draw(plotables, pad=c, logx=True, logy=True, logy_crop_value=1e-11, ylimits=(0.4,800), xlimits=(cfg['min_pt'],cfg['max_pt']+1))

                print(h.name)
                if cfg['prelim']:
                    R.ATLASLabel(0.2,0.88,"Preliminary",1,0.05, 0.2);
                else:
                    R.ATLASLabel(0.2,0.88,"Internal",1,0.06, 0.23);
                R.myText(0.2,0.83,1,"#sqrt{{#it{{s}}}} = 13 TeV, {0:.0f} fb^{{-1}}".format(self.data.lumi+0.03),0.04);
                R.myText(0.2,0.78,1, "{0:.2f} < |#it{{y}}| < {1:.2f}".format(cfg['bins_y'][iy],cfg['bins_y'][iy+1]),0.04);
                R.myText(0.2,0.73,1,' '.join(cfg['par_extra'][par_name][0].split()[:2]),0.04);
                R.myText(0.2,0.69,1,' '.join(cfg['par_extra'][par_name][0].split()[2:]),0.04);

                R.myMarkerLineText(0.71,0.90,0,R.kBlack,0,R.kBlack,1,"Total Uncertainty", 0.03)
                R.myMarkerLineText(0.71,0.86,0,6,0,6,2,"Total Systematics", 0.03)
                R.myOnlyBoxText(0.75,0.82,0.05,8,8,1,"Statistical", 0.03,3004 )
                R.myMarkerLineText(0.75,0.78,0,2,0,2,10,"Fit Model", 0.03)
                if 'N_1S' in par_name or 'N_2S' in par_name:
                    R.myMarkerLineText(0.75,0.74,0,R.kOrange+7,0,R.kOrange+7,7,"Muon Reco", 0.03)
                    R.myMarkerLineText(0.75,0.70,0,R.kRed+2,0,R.kRed+2,6,"Trigger", 0.03)
                    R.myMarkerLineText(0.75,0.66,0,4,0,4,8,"Bin Migration", 0.03)

                figname = '{0}{1}_{2}.pdf'.format(cfg['path_errors'],self.data.year,c.name)
                figname_root = '{0}{1}_{2}.root'.format(cfg['path_errors'],self.data.year,c.name)
                c.save_as(figname)
                c.save_as(figname_root)
                try:
                    self.fig_errors[par_name].append(figname[7:])
                except KeyError:
                    self.fig_errors[par_name] = []
                    self.fig_errors[par_name].append(figname[7:])

    def draw_pt_with_all_errors(self, sys=False):
        """
        Draw pt dependent plots with applied corrections and all uncertainties
        """
        R.gStyle.SetEndErrorSize(5)
        fig_pt = {}

        graph_store = self.data.graphs_pt.clone()
        graphs_pt = graph_store.store

        f_bin = root_open(cfg['bmgr_corr'])
        f_sf = root_open(cfg['sf_corr'])
        f_clos = root_open(cfg['clos_corr'])

        for par_name in cfg['par_extra']:
            graphs = graphs_pt[par_name]
            graphs_sys = []

            #read par_table from config
            title = cfg['par_table'][par_name]['y_title']
            logy  = cfg['par_table'][par_name]['logy']
            mult  = cfg['par_table'][par_name]['scale']
            miny  = cfg['par_table'][par_name]['miny']
            maxy  = cfg['par_table'][par_name]['maxy']

            if par_name in cfg['par_extra']:
                descr = cfg['par_extra'][par_name][0]
                text  = cfg['par_extra'][par_name][1]

            c = Canvas(name=par_name, width=800, height=600)

            colors = ['black','red','blue']
            for i, graph in enumerate(graphs):
                iy = get_iy(graph.name)

                g_bmgr = f_bin['bmgr_corr_y{0}'.format(iy)]
                h_sf_reco = asrootpy(f_sf['sf_reco'].projection_y('rpy_stat',iy+1,iy+1))
                h_sf_trig = asrootpy(f_sf['sf_trig'].projection_y('tpy_stat',iy+1,iy+1))
                h_clos_reco = asrootpy(f_clos['muonreco'].projection_y('rpy_clos',iy+1,iy+1))
                h_clos_trig = asrootpy(f_clos['triggerreco'].projection_y('tpy_clos',iy+1,iy+1))

                if 'N_1S' in graph.name or 'N_2S' in graph.name:
                    for p in graph:
                        #FIXME
                        if p.idx_ < 23:
                            continue
                        #print(p.x.value,g_bmgr[p.idx_].x.value,h_sf_reco[p.idx_-22].x.center,h_clos_trig[p.idx_-22].x.center)
                        corr_factor = 1
                        corr_factor *= g_bmgr[p.idx_].y.value
                        corr_factor *= h_sf_reco[p.idx_-22].value
                        corr_factor *= h_sf_trig[p.idx_-22].value
                        corr_factor *= h_clos_reco[p.idx_-22].value
                        corr_factor *= h_clos_trig[p.idx_-22].value

                        p.y.value     *= corr_factor
                        p.y.error_hi  *= corr_factor
                        p.y.error_low *= corr_factor

                is_jpsi = not '2S' in graph.name and not 'R_NP' in graph.name and not 'R_P' in graph.name
                if not is_jpsi:
                    graph = graph.Crop(cfg['min_pt'], 140)
                else:
                    graph = graph.Crop(cfg['min_pt'], cfg['bins_pt'][-1])

                if mult:
                    graph *= 10**i
                    if sys:
                        graph_sys *= 10**i
                    maxy *= 10
                #graph.color = colors(i)
                graph.color = colors[i]

                graph.markersize = 1.3
                graph.drawstyle = 'P'

                graph_sys = graph.clone()
                graph_sys.name = graph.name+'_err'
                h_error = self.errors[par_name][iy]
                for p in graph_sys:
                    p.y.error_hi = p.y.value*h_error[p.idx_+24].value*1e-2
                    p.y.error_low = p.y.error_hi
                graph_sys.drawstyle = 'PZ'
                graphs_sys.append(graph_sys)

            plotables = graphs[:]
            plotables.extend(graphs_sys)
            graph0 = graphs[0].clone()
            plotables.insert(0,graph0)

            graph0.drawstyle = ''
            graph0.color = 'white'
            graph0.xaxis.title = "#it{p}_{T}(#mu#mu) [GeV]"
            graph0.xaxis.set_title_size(0.05)
            graph0.xaxis.set_label_size(0.05)
            graph0.yaxis.title = title
            graph0.yaxis.set_title_size(0.05)
            graph0.yaxis.set_label_size(0.05)
            graph0.yaxis.set_title_offset(1.5)
            graph0.xaxis.set_more_log_labels(True)
            graph0.xaxis.set_no_exponent(True)

            #FIXME
            if is_jpsi or 'f_NP' in par_name:
                if 'f_NP' in par_name:
                    draw(plotables, pad=c, yerror_in_padding=False, logx=True, logy=logy, logy_crop_value=1e-11, ylimits=(0,1), xlimits=(cfg['min_pt']-1,cfg['max_pt']+1))
                else:
                    draw(plotables, pad=c, yerror_in_padding=False, logx=True, logy=logy, logy_crop_value=1e-11, ypadding=(0.2,0.1), xlimits=(cfg['min_pt']-1,cfg['max_pt']+1))
            else:
                if 'R_' in par_name:
                    draw(plotables, pad=c, yerror_in_padding=False, logx=True, logy=logy, logy_crop_value=1e-11, ylimits=(0,0.14), xlimits=(cfg['min_pt']-1,cfg['max_pt']+1))
                else:
                    draw(plotables, pad=c, yerror_in_padding=False, logx=True, logy=logy, logy_crop_value=1e-11, ypadding=(0.3,0.1), xlimits=(cfg['min_pt']-1,cfg['max_pt']+1))

            if 'f_NP' in par_name or 'R_' in par_name:
                if cfg['prelim']:
                    R.ATLASLabel(0.52,0.88,"Preliminary",1,0.06, 0.18);
                    R.myText(0.52,0.83,1,"#sqrt{{#it{{s}}}} = 13 TeV, {0:.0f} fb^{{-1}}".format(self.data.lumi+0.03),0.05);
                else:
                    R.ATLASLabel(0.6,0.88,"Internal",1,0.06, 0.18);
                    R.myText(0.6,0.83,1,"#sqrt{{#it{{s}}}} = 13 TeV, {0:.0f} fb^{{-1}}".format(self.data.lumi+0.03),0.05);
            else:
                if cfg['prelim']:
                    R.ATLASLabel(0.2,0.88,"Preliminary",1,0.05, 0.15);
                else:
                    R.ATLASLabel(0.2,0.88,"Internal",1,0.06, 0.18);
                R.myText(0.2,0.83,1,"#sqrt{{#it{{s}}}} = 13 TeV, {0:.0f} fb^{{-1}}".format(self.data.lumi+0.03),0.05);
            if text is not None:
                R.myText(0.6,0.90,1,text,0.05);
            for i, graph in reversed(list(enumerate(graphs))):
                color = base.convert_color(graph.linecolor, 'root')
                iy = get_iy(graph.name)
                if mult:
                    R.myMarkerLineText(0.59,0.83-0.05*(len(graphs)-1-i),1.3,color,20,color,1,"data #times 10^{{{0}}}, {1:.2f} < |#it{{y}}| < {2:.2f}".format(
                        i, cfg['bins_y'][i],cfg['bins_y'][i+1]), 0.04)
                else:
                    R.myMarkerLineText(0.65,0.36-0.06*(len(graphs)-1-i),1.3,color,20,color,1,"{1:.2f} < |#it{{y}}| < {2:.2f}".format(
                        i, cfg['bins_y'][i],cfg['bins_y'][i+1]), 0.05)

            figname = '{0}{1}_{2}_corr.pdf'.format(cfg['path_pt_graphs'],self.data.year,c.name)
            figname_root = '{0}{1}_{2}_corr.root'.format(cfg['path_pt_graphs'],self.data.year,c.name)
            c.save_as(figname)
            c.save_as(figname_root)

            fig_pt[par_name] = figname[7:]
        return fig_pt

    def draw_pt(self, sys=False):
        """
        Draw pt dependent plots
        """
        fig_pt = {}

        graph_store = self.data.graphs_pt.clone()
        graphs_pt = graph_store.store

        if sys:
            gstore = self.data.apply_fitsys()[0]
            graphs_varied = gstore.store

        for par_name in cfg['par_table']:
            if 'Chi2' in par_name:
                continue
            if par_name not in graphs_pt:
                continue

            graphs = graphs_pt[par_name]
            if sys:
                graphs_sys = graphs_varied[par_name]

            #read par_table from config
            title = cfg['par_table'][par_name]['y_title']
            logy  = cfg['par_table'][par_name]['logy']
            mult  = cfg['par_table'][par_name]['scale']
            miny  = cfg['par_table'][par_name]['miny']
            maxy  = cfg['par_table'][par_name]['maxy']

            if par_name in cfg['par_extra']:
                descr = cfg['par_extra'][par_name][0]
                text  = cfg['par_extra'][par_name][1]

            if not sys:
                c = Canvas(name=par_name, width=800, height=600)
            else:
                c = Canvas(name=par_name+'_sys', width=800, height=600)

            #colors = get_colors(len(graphs))
            colors = ['black','red','blue']
            for i, graph in enumerate(graphs):
                is_jpsi = not '2S' in graph.name and not 'R_NP' in graph.name and not 'R_P' in graph.name
                if not is_jpsi:
                    graph = graph.Crop(cfg['min_pt'], 140)
                else:
                    graph = graph.Crop(cfg['min_pt'], cfg['bins_pt'][-1])
                if sys:
                    graph_sys = graphs_sys[i]
                    is_jpsi = not '2S' in graph.name and not 'R_NP' in graph.name and not 'R_P' in graph.name
                    if not is_jpsi:
                        graph_sys = graph_sys.Crop(cfg['min_pt'], 140)
                    else:
                        graph_sys = graph_sys.Crop(cfg['min_pt'], cfg['bins_pt'][-1])
                    graph_sys.drawstyle = '2'
                    graph_sys.fillstyle = '\\'
                    graph_sys.fillcolor = colors[i]
                min_old = graph.get_minimum()
                max_old = graph.get_maximum()
                for point in graph:
                    if min_old == 0:
                        continue
                    g_clone = graph.clone()
                    g_clone.remove_point(point.idx_)
                    min_new = g_clone.get_minimum()
                    max_new = g_clone.get_maximum()
                    if (min_new / min_old) > 1e4:
                        #move point up with big error:
                        #point.y.value = g_clone.get_mean(2)
                        point.y.value = 1.1*min_new
                        if sys:
                            graph_sys[point.idx_].y.value = 1.1* min_new
                        point.y.error_hi = point.y.value
                        point.y.error_low = point.y.value

                        min_old = graph.get_minimum()

                    if max_new == 0:
                        continue
                    if not logy and (max_old / max_new) > 4:
                        #move point down with big error:
                        #point.y.value = g_clone.get_mean(2)
                        point.y.value = 0.9*max_new
                        if sys:
                            graph_sys[point.idx_].y.value = 0.9* max_new
                        point.y.error_hi = point.y.value
                        point.y.error_low = point.y.value

                        max_old = graph.get_maximum()

                for point in graph:
                    if point.y.value:
                        break
                if mult:
                    graph *= 10**i
                    if sys:
                        graph_sys *= 10**i
                    maxy *= 10
                #graph.color = colors(i)
                graph.color = colors[i]
                #FIXME
                #graph = graph.Crop(bins_pt[point.idx_], bins_pt[-1])

                graph.markersize = 1.3
                graph.drawstyle = 'P'

            plotables = graphs[:]
            graph0 = graphs[0].clone()
            plotables.insert(0,graph0)
            if sys and par_name in cfg['par_extra']:
                for i, g in enumerate(graphs_sys):
                    plotables.insert(i+1, g)

            graph0.drawstyle = ''
            graph0.color = 'white'
            graph0.xaxis.title = "#it{p}_{T}(#mu#mu) [GeV]"
            graph0.yaxis.title = title
            graph0.yaxis.set_title_size(0.04)
            graph0.yaxis.set_title_offset(1.55)
            graph0.xaxis.set_more_log_labels(True)
            graph0.xaxis.set_no_exponent(True)

            draw(plotables, pad=c, yerror_in_padding=False, logx=True, logy=logy, logy_crop_value=1e-11, ypadding=(0.3,0.1), xlimits=(cfg['min_pt']-1,cfg['max_pt']+1))

            if cfg['prelim']:
                R.ATLASLabel(0.2,0.88,"Preliminary");
            else:
                R.ATLASLabel(0.2,0.88,"Internal");
            R.myText(0.2,0.83,1,"#sqrt{{#it{{s}}}} = 13 TeV, {0:.0f} fb^{{-1}}".format(self.data.lumi+0.03));
            if text is not None:
                R.myText(0.2,0.78,1,text);
            for i, graph in reversed(list(enumerate(graphs))):
                color = base.convert_color(graph.linecolor, 'root')
                iy = get_iy(graph.name)
                if mult:
                    R.myMarkerLineText(0.59,0.90-0.05*(len(graphs)-1-i),1.3,color,20,color,1,"data #times 10^{{{0}}}, {1:.2f} < |#it{{y}}| < {2:.2f}".format(
                        i, cfg['bins_y'][i],cfg['bins_y'][i+1]), 0.04)
                else:
                    R.myMarkerLineText(0.69,0.90-0.05*(len(graphs)-1-i),1.3,color,20,color,1,"{1:.2f} < |#it{{y}}| < {2:.2f}".format(
                        i, cfg['bins_y'][i],cfg['bins_y'][i+1]), 0.04)
            if sys:
                if mult:
                    R.myOnlyBoxText(0.59,0.90-0.05*(len(graphs)-i),0.05,1,1,1,"Fit syst. uncert", 0.04,3345 )
                else:
                    R.myOnlyBoxText(0.69,0.90-0.05*(len(graphs)-i),0.05,1,1,1,"Fit syst. uncert", 0.04,3345 )

            figname = '{0}{1}_{2}.pdf'.format(cfg['path_pt_graphs'],self.data.year,c.name)
            figname_root = '{0}{1}_{2}.root'.format(cfg['path_pt_graphs'],self.data.year,c.name)
            c.save_as(figname)
            c.save_as(figname_root)

            fig_pt[par_name] = figname[7:]
        return fig_pt

    def draw_y(self):
        """
        Draw y dependent plots
        """
        graphs_y = self.data.graphs_y.store

        for par_name in cfg['par_table']:
            if 'Chi2' in par_name:
                continue
            if par_name not in graphs_y:
                continue

            graphs = graphs_y[par_name]

            #Filter empty graphs
            selected = []
            for graph in graphs:
                #FIXME
                ipt = get_ipt(graph.name)
                if cfg['bins_pt'][ipt] < cfg['min_pt']:
                    continue
                is_jpsi = not '2S' in graph.name and not 'R_NP' in graph.name and not 'R_P' in graph.name
                if not is_jpsi:
                    if cfg['bins_pt'][ipt] >= 140:
                        continue
                selected.append(graph)

            graphs = selected

            #read par_table from config
            title = cfg['par_table'][par_name]['y_title']
            logy  = cfg['par_table'][par_name]['logy']
            mult  = cfg['par_table'][par_name]['scale']
            miny  = cfg['par_table'][par_name]['miny']
            maxy  = cfg['par_table'][par_name]['maxy']

            if par_name in cfg['par_extra']:
                descr = cfg['par_extra'][par_name][0]
                text  = cfg['par_extra'][par_name][1]

            c = Canvas(name=par_name, width=800, height=1064)

            colors = get_colors(len(graphs))
            for i, graph in enumerate(graphs):
                graph.color = colors(i)
                graph.markersize = 1.5
                graph.drawstyle = 'P'
                if i%2:
                    graph.markerstyle = 'square'

            plotables = graphs[:]
            graph0 = graphs[0].clone()
            plotables.insert(0,graph0)

            graph0.drawstyle = ''
            graph0.color = 'white'
            graph0.xaxis.title = "|y(#mu#mu)|"
            graph0.yaxis.title = title
            graph0.yaxis.set_title_size(0.04)
            graph0.yaxis.set_label_size(0.04)
            graph0.yaxis.set_title_offset(1.55)
            graph0.xaxis.set_more_log_labels(True)
            graph0.xaxis.set_title_size(0.04)
            graph0.xaxis.set_label_size(0.04)

            draw(plotables, pad=c, yerror_in_padding=False, logy=logy, logy_crop_value=1e-11, ypadding=(0.65,0.05))

            R.ATLASLabel(0.2,0.88,"Internal", 1, 0.04, 0.15);
            R.myText(0.2,0.84,1,"#sqrt{{#it{{s}}}} = 13 TeV, {0:.0f} fb^{{-1}}".format(self.data.lumi+0.03), 0.03);
            if text is not None:
                R.myText(0.2,0.79,1,text);
            for i, graph in reversed(list(enumerate(graphs))):
                color = base.convert_color(graph.linecolor, 'root')
                marker = base.convert_markerstyle(graph.markerstyle, 'root')
                ipt = get_ipt(graph.name)
                R.myMarkerLineText(0.60,0.90-0.04*(len(graphs)-1-i),1.3,color,marker,color,1,"data , {0:.2f} < p_{{T}} < {1:.2f}".format(
                    cfg['bins_pt'][ipt],cfg['bins_pt'][ipt+1]), 0.03);

            figname = '{0}{1}_{2}.pdf'.format(cfg['path_y_graphs'],self.data.year,c.name)
            figname_root = '{0}{1}_{2}.root'.format(cfg['path_y_graphs'],self.data.year,c.name)
            c.save_as(figname)
            c.save_as(figname_root)

            self.fig_y[par_name] = figname[7:]

    def draw_maps(self):
        """
        Draw 2D parameter maps
        """
        R.set_plot_style()
        maps = self.data.maps.store
        for par_name in cfg['par_table']:
            if 'Chi2' in par_name:
                continue
            if par_name not in maps:
                continue

            map_vals = maps[par_name][0]
            map_errs = maps[par_name][1]

            map_vals.yaxis.set_range_user(cfg['bins_pt'].index(cfg['min_pt']), cfg['bins_pt'].index(cfg['max_pt']))
            map_errs.yaxis.set_range_user(cfg['bins_pt'].index(cfg['min_pt']), cfg['bins_pt'].index(cfg['max_pt']))

            self.maps[par_name] = []

            c = Canvas(name=par_name, width=800, height=600)
            c.set_right_margin(0.15)
            c.set_top_margin(0.1)
            #c.set_logy(True)
            map_vals.draw('colz text')
            figname = '{0}{1}_{2}.pdf'.format(cfg['path_par_2D'],self.data.year,c.name)
            c.save_as(figname)

            self.maps[par_name].append(figname[7:])

            c.name = par_name+'_Err'
            map_errs.draw('colz text')
            figname = '{0}{1}_{2}.pdf'.format(cfg['path_par_2D'],self.data.year,c.name)
            c.save_as(figname)

            self.maps[par_name].append(figname[7:])

    def draw_year(self):
        """
        Draw par vs year plots
        """
        par_graphs = self.data.par_graph
        year_graphs = {}

        for par_name in cfg['par_extra']:
            if par_name not in [k[0] for k in par_graphs.keys()]:
                continue
            for iy in range(len(cfg['bins_y'])-1):
                #for ipt in range(len(bins_pt)-1):
                for ipt in range(cfg['bins_pt'].index(cfg['min_pt']),len(cfg['bins_pt'])-1):
                    graph = par_graphs[par_name,iy,ipt]

                    #Filter empty
                    #empty = False
                    #for point in graph:
                        #if not point.y.value:
                            #empty = True
                            #break
#
                    #if empty:
                        #continue

                    #read par_table from config
                    title = cfg['par_table'][par_name]['y_title']
                    logy  = cfg['par_table'][par_name]['logy']
                    mult  = cfg['par_table'][par_name]['scale']
                    miny  = cfg['par_table'][par_name]['miny']
                    maxy  = cfg['par_table'][par_name]['maxy']

                    descr = cfg['par_extra'][par_name][0]
                    text  = cfg['par_extra'][par_name][1]

                    c = Canvas(name="{0}_y{1}_pt{2}".format(par_name,iy,ipt), width=800, height=600)
                    c.draw()

                    graph.markersize = 1.8
                    graph.color = 'black'
                    graph.drawstyle = 'P'

                    graph0 = graph.clone()

                    graph0.drawstyle = ''
                    graph0.color = 'white'
                    graph0.xaxis.set_ndivisions(graph.num_points)
                    graph0.xaxis.title = "Year"
                    graph0.yaxis.title = title
                    graph0.yaxis.set_title_size(0.04)
                    graph0.yaxis.set_title_offset(1.55)
                    graph0.xaxis.set_more_log_labels(True)

                    f = graph.get_function('pol0')
                    f.SetLineColor(2)
                    val = f.GetParameter(0)
                    err = f.GetParError(0)
                    chi2 = f.GetChisquare()

                    draw([graph0,graph], pad=c, yerror_in_padding=False, ypadding=(0.6,0.1), xpadding=0.1)

                    R.myText(0.45,0.70,2,"{0:.2e} #pm {1:.2e}".format(val, err))
                    R.myText(0.45,0.65,2,"#chi^{{2}}/ndf: {0:.2f}".format(chi2/3))

                    R.ATLASLabel(0.2,0.88,"Internal");
                    R.myText(0.2,0.84,1,"#sqrt{#it{s}} = 13 TeV")
                    R.myText(0.2,0.79,1,text);

                    R.myText(0.67,0.88,1,"{0:.2f} < p_{{T}} < {1:.2f}".format(cfg['bins_pt'][ipt],cfg['bins_pt'][ipt+1]));
                    R.myText(0.67,0.83,1, "{0:.2f} < |#it{{y}}| < {1:.2f}".format(cfg['bins_y'][iy],cfg['bins_y'][iy+1]));

                    figname = '{0}{1}.pdf'.format(cfg['path_year_graphs'],c.name)
                    figname_root = '{0}{1}.root'.format(cfg['path_year_graphs'],c.name)
                    c.save_as(figname)
                    c.save_as(figname_root)

                    try:
                        year_graphs[par_name].append(figname[7:])
                    except:
                        year_graphs[par_name] = []
                        year_graphs[par_name].append(figname[7:])

        return year_graphs

    def fitresults(self):
        """
        Draw fitted distributions - m and tau projections
        """
        with root_open(self.data.fithisto_path) as f:
            for ipt in range(cfg['bins_pt'].index(cfg['min_pt']),cfg['bins_pt'].index(cfg['max_pt'])):
                for iy in range(3):
                    canvas_m = f["mass_y{0}_pt{1}_sys0".format(iy,ipt)]

                    #remove chi2 text and modify lumi value
                    text = [i for i in asrootpy(canvas_m.primitives[0]).primitives if i.__class__.__name__ == "TText"][0]
                    lumi_obj = [i for i in asrootpy(canvas_m.primitives[0]).primitives if i.__class__.__name__ == "TLatex"][2]
                    lumi_text = lumi_obj.GetTitle()
                    lumi_text = lumi_text.split(',')[0]+', {0:.0f} fb^{{-1}}'.format(self.data.lumi+0.03)
                    lumi_obj.SetTitle(lumi_text)
                    chi2 = float(text.GetTitle().split()[2])
                    text.SetTitle("")
                    #R.myText(0.20,0.65,2,"#chi^{{2}}/ndof: {0:.2f}".format(chi2))
                    #Change xaxis title
                    h = asrootpy(asrootpy(canvas_m.primitives[1]).primitives[0].item)
                    h.xaxis.title = '#it{m}(#mu#mu) [GeV]'

                    #Save
                    figname_m = '{0}{1}_{2}.pdf'.format(cfg['path_fitted'],self.data.year,canvas_m.name)
                    canvas_m.save_as(figname_m)

                    canvas_tau = f["tau_y{0}_pt{1}_sys0".format(iy,ipt)]
                    canvas_tau.cd()

                    #remove chi2 text and modify the lumi value
                    text = [i for i in asrootpy(canvas_tau.primitives[0]).primitives if i.__class__.__name__ == "TText"][0]
                    lumi_obj = [i for i in asrootpy(canvas_tau.primitives[0]).primitives if i.__class__.__name__ == "TLatex"][2]
                    lumi_text = lumi_obj.GetTitle()
                    lumi_text = lumi_text.split(',')[0]+', {0:.0f} fb^{{-1}}'.format(self.data.lumi+0.03)
                    lumi_obj.SetTitle(lumi_text)
                    chi2 = float(text.GetTitle().split()[2])
                    text.SetTitle("")
                    #R.myText(0.70,0.76,2,"#chi^{{2}}/ndof: {0:.2f}".format(chi2))
                    #Change xaxis title
                    h = asrootpy(asrootpy(canvas_tau.primitives[1]).primitives[0].item)
                    h.xaxis.title = '#tau(#mu#mu) [ps]'

                    #Save
                    figname_tau = '{0}{1}_{2}.pdf'.format(cfg['path_fitted'],self.data.year,canvas_tau.name)
                    canvas_tau.save_as(figname_tau)

                    self.fitted.append((figname_m[7:], figname_tau[7:]))


