from __future__ import absolute_import

import re
from collections import OrderedDict
from copy import deepcopy
import numpy

import rootpy.ROOT as R
from rootpy.io import root_open, DoesNotExist
from rootpy.plotting.utils import Compare
from rootpy.plotting import Canvas, Graph, Hist2D
from rootpy import log

from .utils import *

__all__ = [
        'OniaData',
        'OniaCombData'
        ]

#Read config
cfg = load_cfg()

class OniaData(object):
    def __init__(self, year, par_path, fithisto_path, ntuples, apply_acc = True):
        self.year = year
        self.lumi = cfg['lumi_dict'][year]*1e-6
        self.par_path = par_path
        self.fithisto_path = fithisto_path
        self.graphs_pt = RooParameters(par_path).GraphStore
        log.info("Systematic variations: {0}".format(self.graphs_pt.sys_set))
        if not isinstance(ntuples, list):
            self.ntuples = [ntuples]
        else:
            self.ntuples = ntuples
        if apply_acc:
            for i in range(2):
                try:
                    with root_open('{0}Acceptance_{1}.root'.format(cfg['path_rootfiles'], self.year)) as acc_file:
                        log.info(
                                "Loading previously generated acceptance weights from {0}".format(
                                    cfg['path_rootfiles']+'Acceptance.root'))
                        self.acc_map_1S = acc_file.psi_1S_flat
                        self.acc_map_2S = acc_file.psi_2S_flat
                        self.acc_map_1S.SetDirectory(0)
                        self.acc_map_2S.SetDirectory(0)
                    break
                except:
                    self.prepare_acc()
        #Correct using acceptance weights and calculate xsection and ratios
        self.append_pars()
        self.sys_graphs_pt = None
        self.sys_graphs_frac_uncert = None
        #self.produce_2D()

    @property
    def year(self):
        return self.__year

    @year.setter
    def year(self, year):
        if not isinstance(year, int):
            raise TypeError("year should be of type int")
        else:
            self.__year = year

    @property
    def par_path(self):
        return self.__par_path

    @par_path.setter
    def par_path(self, par_path):
        if not isinstance(par_path, str):
            raise TypeError("par_path should be of type str")
        else:
            self.__par_path = par_path

    @property
    def fithisto_path(self):
        return self.__fithisto_path

    @fithisto_path.setter
    def fithisto_path(self, fithisto_path):
        if not isinstance(fithisto_path, str):
            raise TypeError("fithisto_path should be of type str")
        else:
            self.__fithisto_path = fithisto_path

    def prepare_acc(self):
        """
        Prepare maps for J/psi and psi'
        """
        acc_1S = root_open(cfg['acc_file_1S'])
        acc_2S = root_open(cfg['acc_file_2S'])

        pols = ['flat', 'long', 'trp0', 'trpp', 'trpm', 'offP', 'offN']

        for pol in pols:
            try:
                input_map_1S = acc_1S[pol]
                input_map_2S = acc_2S[pol]
            except DoesNotExist:
                log.info('Skipping {0} polarization, no input map.'.format(pol))

            log.info(
                    "Performing mean acceptance weights calculation of J/psi "\
                    "for {0} polarization...".format(pol))
            acc_map_1S = self.generate_acc(input_map_1S, True)
            log.info(
                    "Performing mean acceptance weights calculation of psi(2S) "\
                    "for {0} polarization...".format(pol))
            acc_map_2S = self.generate_acc(input_map_2S, False)

            c = Canvas(name="Acc_1S_2D_{0}".format(pol), width=800, height=600)
            c.set_right_margin(0.15)
            c.set_top_margin(0.1)
            c.draw()
            acc_map_1S.draw('colztext')
            c.save_as(cfg['path_weight_2D']+c.name+'.pdf')

            c = Canvas(name="Acc_2S_2D_{0}".format(pol), width=800, height=600)
            c.set_right_margin(0.15)
            c.set_top_margin(0.1)
            c.draw()
            acc_map_2S.draw('colztext')
            c.save_as(cfg['path_weight_2D']+c.name+'.pdf')

            acc_map_1S.name = 'psi_1S_{0}'.format(pol)
            acc_map_2S.name = 'psi_2S_{0}'.format(pol)

            with root_open('{0}Acceptance_{1}.root'.format(cfg['path_rootfiles'], self.year),'a') as acc_file:
                acc_map_1S.write()
                acc_map_2S.write()

    def generate_acc(self, acc_map, is_jpsi):
        R.TH1.SetDefaultSumw2(True)
        out_map = Hist2D(len(cfg['bins_y'])-1, 0, len(cfg['bins_y'])-1, len(cfg['bins_pt'])-1, 0, len(cfg['bins_pt'])-1)
        out_map.yaxis.title = "#it{p}_{T} bin"
        out_map.xaxis.title = "|y| bin"
        out_map.xaxis.set_ndivisions(len(cfg['bins_y'])-1)
        #out_map.yaxis.set_ndivisions(len(cfg['bins_pt'])-1)
        out_map.zaxis.title = 'Average Acceptance'
        for iy, (y_lo, y_hi) in enumerate(zip(cfg['bins_y'], cfg['bins_y'][1:])):
            g_sigma = self.graphs_pt['y{0}_Mass_Sigma_1S_G'.format(iy)]
            g_mean = self.graphs_pt['y{0}_Mass_Mean_1S'.format(iy)]
            for ipt, (pt_lo, pt_hi) in enumerate(zip(cfg['bins_pt'], cfg['bins_pt'][1:])):
                if pt_lo < 60: #Acceptance is for mu4mu52p5
                    continue

                peak = g_mean[ipt].y.value
                sigma = g_sigma[ipt].y.value
                if not  is_jpsi:
                    peak *= 3.686/3.097
                    sigma *= 3.686/3.097

                w_acc = []

                for i, ntuple in enumerate(self.ntuples):
                    with root_open(ntuple) as f:
                        tree = f['tuple_y{0}_pt{1}'.format(iy, ipt)]
                        for e in tree:
                            if not (e.passTrigger & cfg['trigger_cut'][self.year][i]):
                                continue
                            if e.weight_reco >= 20 or not e.weight_trig_single:
                                continue
                            if e.pt1<52.5 and e.pt2<52.5:
                                continue
                            if not abs(e.m - peak) < 6*sigma:
                                continue

                            bin_no = acc_map.find_bin(abs(e.y), e.pt)
                            w_acc.append(1/acc_map[bin_no].value)

                out_map[iy+1, ipt+1].value = numpy.mean(w_acc)
        R.TH1.SetDefaultSumw2(False)
        return out_map

    def append_pars(self, sys_no=0):
        """
        Apply acceptance weights, calculate luminosity and produce ratio plots
        """
        if sys_no == 0:
            N_1S_P = 'N_1S_P'
            N_1S_NP = 'N_1S_NP'
            N_2S_P = 'N_2S_P'
            N_2S_NP = 'N_2S_NP'
        else:
            N_1S_P = 'N_1S_P_sys{0}'.format(sys_no)
            N_1S_NP = 'N_1S_NP_sys{0}'.format(sys_no)
            N_2S_P = 'N_2S_P_sys{0}'.format(sys_no)
            N_2S_NP = 'N_2S_NP_sys{0}'.format(sys_no)

        #J/psi
        graphs_1S_P = self.graphs_pt.store[N_1S_P]
        graphs_1S_NP = self.graphs_pt.store[N_1S_NP]
        #psi'
        graphs_2S_P = self.graphs_pt.store[N_2S_P]
        graphs_2S_NP = self.graphs_pt.store[N_2S_NP]

        log.info("Performing cross-section calculation")

        self.calc_xsec(graphs_1S_P)
        self.calc_xsec(graphs_1S_NP)

        self.calc_xsec(graphs_2S_P)
        self.calc_xsec(graphs_2S_NP)

        log.info("Performing ratio calculation")
        for g_2S, g_1S in zip(graphs_2S_P, graphs_1S_P):
            g = Compare({'x0':g_2S, 'x1':g_1S}, 'x0/x1').results[0]
            iy = get_iy(g_1S.name)
            if sys_no == 0:
                name = 'graph_y{0}_R_P'.format(iy)
            else:
                name = 'graph_y{0}_R_P_sys{1}'.format(iy, sys_no)
            g.name = name
            self.graphs_pt.insert(g)

        for g_2S, g_1S in zip(graphs_2S_NP, graphs_1S_NP):
            g = Compare({'x0':g_2S, 'x1':g_1S}, 'x0/x1').results[0]
            iy = get_iy(g_1S.name)
            if sys_no == 0:
                name = 'graph_y{0}_R_NP'.format(iy)
            else:
                name = 'graph_y{0}_R_NP_sys{1}'.format(iy, sys_no)
            g.name = name
            self.graphs_pt.insert(g)

    def calc_xsec(self, graphs):
        """
        Apply acceptance correction on yields and divide by (lumi * 2D bin size)
        graphs:  A list of output graphs from utils.RooParameters
        """
        for graph in graphs:
            is_jpsi = '1S' in graph.name
            is_pt = '_y' in graph.name #pT dependent or y dependent

            if is_jpsi:
                try:
                    acc_map = self.acc_map_1S
                except AttributeError:
                    acc_map = None
            else:
                try:
                    acc_map = self.acc_map_2S
                except AttributeError:
                    acc_map = None

            if is_pt:
                iy = get_iy(graph.name)
                y_lo = cfg['bins_y'][iy]
                y_hi = cfg['bins_y'][iy+1]
                dy = y_hi - y_lo
            else:
                ipt = get_ipt(graph.name)
                pt_lo = cfg['bins_pt'][ipt]
                pt_hi = cfg['bins_pt'][ipt+1]
                dpt = pt_hi - pt_lo

            for point in graph:
                if not point.y.value:
                    continue
                if is_pt:
                    ipt = point.idx_
                    pt_lo = (point.x.value-point.x.error_low)
                    pt_hi = (point.x.value+point.x.error_hi)
                    dpt = pt_hi - pt_lo
                else:
                    iy = point.idx_
                    y_lo = (point.x.value-point.x.error_low)
                    y_hi = (point.x.value+point.x.error_hi)
                    dy = y_hi - y_lo

                if acc_map is not None:
                    w_acc = acc_map[iy+1, ipt+1].value
                else:
                    #No acceptance weighting was requested
                    w_acc = 1
                factor = self.lumi*2*dy*dpt/w_acc

                #point.y.value = point.y.value*10**i/factor
                point.y.value = point.y.value/factor
                point.y.error_hi = point.y.error_hi/factor
                point.y.error_low = point.y.error_low/factor

    def produce_graphs_y(self):
        """
        Produce y dependent graphs from pt dependent graphs
        """
        self.graphs_y = pt2y(self.graphs_pt)

    def produce_maps(self):
        """
        Produces 2D maps from graphs
        """
        self.maps = GraphStore2MapStore(self.graphs_pt)

    def apply_fitsys(self):
        """
        Fit model systematics
        """
        graphs_pt = self.graphs_pt.clone()
        if self.sys_graphs_pt is not None and self.sys_graphs_frac_uncert is not None:
            return (self.sys_graphs_pt, self.sys_graphs_frac_uncert)

        for sys_no in self.graphs_pt.sys_set:
            self.append_pars(sys_no)

        graphs_frac_uncert = self.graphs_pt.clone()

        for par_name in cfg['par_extra']:
            graphs_nom = graphs_pt.store[par_name]
            graphs_sys = [
                    self.graphs_pt.store['{0}_sys{1}'.format(par_name, i)] for i in self.graphs_pt.sys_set ]
            for g in graphs_nom:
                iy = get_iy(g.name)
                sys_collection = []
                for graphs in graphs_sys:
                    sys_collection.append(graphs[iy])

                for point in g:
                    nom = point.y.value
                    if nom == 0:
                        continue
                    sys_err_up = [0]
                    sys_err_down = [0]
                    for g_sys in sys_collection:
                        #FIXME
                        if 'sys7' in g_sys.name:
                            continue
                        if point.idx_ == 24 and iy == 2:
                            if 'sys2' in g_sys.name or 'sys3' in g_sys.name or 'sys7' in g_sys.name:
                                continue

                        varied = g_sys[point.idx_].y.value
                        err = varied - nom
                        if err > 0:
                            sys_err_up.append(err)
                        else:
                            sys_err_down.append(-err)

                        fraction = err/nom

                        if fraction > 0.2:
                            fraction = 0.19
                        if fraction < -0.2:
                            fraction = -0.19

                        g_frac = graphs_frac_uncert[g_sys.name[6:]]
                        g_frac[point.idx_].y.value = fraction
                        g_frac[point.idx_].y.error_hi = 0
                        g_frac[point.idx_].y.error_low = 0
                        #g_frac[point.idx_].x.error_hi = 0
                        #g_frac[point.idx_].x.error_low = 0

                    point.y.error_hi = max(sys_err_up)
                    point.y.error_low = max(sys_err_down)

        self.sys_applied = True

        self.sys_graphs_pt = graphs_pt
        self.sys_graphs_frac_uncert = graphs_frac_uncert

        return (graphs_pt, graphs_frac_uncert)

    def __add__(self, other):
        lumi = self.lumi + other.lumi
        graph_dict = OrderedDict([
            (self.year, self.graphs_pt),
            (other.year, other.graphs_pt),
            ])
        return OniaCombData(lumi, graph_dict)

class OniaCombData(object):
    def __init__(self, lumi, graph_dict):
        self.lumi = lumi
        self.graph_dict = graph_dict #GraphStore dict {year:GraphStore}

        #Determining year
        year = ''
        for yr in self.graph_dict:
            year += str(yr)[-2:]
        self.year = int(year)

        self.par_graph = {} #dict for par_vs_x graphs {(par_name,iy,ipt):graph}
        self.init_par_grap()
        graphs_pt = self.graph_dict.values()[0]
        self.graphs_pt = GraphStore(cfg['par_extra'])
        #Remove unneeded parameters
        for par_name in graphs_pt.store:
            if par_name in cfg['par_extra']:
                graphs = graphs_pt.store[par_name]
                for graph in graphs:
                    self.graphs_pt.insert(graph, True)

    @property
    def lumi(self):
        return self.__lumi

    @lumi.setter
    def lumi(self, lumi):
        if not isinstance(lumi, float):
            raise TypeError("lumi should be of type float")
        else:
            self.__lumi = lumi

    def __add__(self, other):
        lumi = self.lumi + other.lumi
        graph_dict = self.graph_dict.copy()
        if isinstance(other, OniaData):
            graph_dict[other.year] = other.graphs_pt
        elif isinstance(other, OniaCombData):
            graph_dict.update(other.graph_dict)

        return OniaCombData(lumi, graph_dict)

    def init_par_grap(self):
        for par_name in cfg['par_extra']:
            for iy in range(len(cfg['bins_y'])-1):
                for ipt in range(len(cfg['bins_pt'])-1):
                    graph = Graph(len(self.graph_dict), name='g_{0}_y{1}_pt{2}'.format(par_name, iy, ipt), type='asymm')
                    self.par_graph[(par_name, iy, ipt)] = graph

    def par_vs_year(self):
        """
        Produce par vs year graphs
        """
        log.info("Producing par vs year graphs")
        for par_name in cfg['par_extra']:
            #graph_out = Graph(len(self.graph_dict))
            for iy in range(len(cfg['bins_y'])-1):
                for i, year in enumerate(self.graph_dict):
                    graph = self.graph_dict[year].store[par_name][iy]
                    for point in graph:
                        graph_out = self.par_graph[(par_name,iy,point.idx_)]

                        graph_out[i] = (year, point.y.value)

                        #graph_out[i].x.error_hi = 0.5
                        #graph_out[i].x.error_low = 0.5

                        if point.y.value < 1e-9:
                            graph_out[i].y.error_hi = 10
                            graph_out[i].y.error_low = 10
                        else:
                            graph_out[i].y.error_hi = point.y.error_hi
                            graph_out[i].y.error_low = point.y.error_low

    def produce_graphs_pt(self):
        """
        Produce pt-dependent graphs from par_vs_year graphs
        """
        log.info("Fitting par vs year graphs")
        for par_name in self.graphs_pt.store:
            graphs = self.graphs_pt.store[par_name]
            for graph in graphs:
                iy = get_iy(graph.name)
                for point in graph:
                    ipt = point.idx_
                    par_graph = self.par_graph[par_name,iy,ipt]
                    fit = par_graph.fit('pol0', 'SQ')
                    f = par_graph.get_function('pol0')
                    val = f.GetParameter(0)
                    err = f.GetParError(0)

                    mean_pt = (cfg['bins_pt'][ipt]+cfg['bins_pt'][ipt+1])/2
                    point.x.value = mean_pt
                    point.x.error_hi = cfg['bins_pt'][ipt+1] - mean_pt
                    point.x.error_low = mean_pt - cfg['bins_pt'][ipt]

                    point.y.value = val
                    point.y.error_hi = err
                    point.y.error_low = err

    def produce_graphs_y(self):
        """
        Produce y dependent graphs from pt dependent graphs
        """
        self.graphs_y = pt2y(self.graphs_pt)

    def produce_maps(self):
        """
        Produces 2D maps from graphs
        """
        self.maps = GraphStore2MapStore(self.graphs_pt)
