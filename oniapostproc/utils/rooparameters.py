from __future__ import absolute_import

import re
import glob
import rootpy.ROOT as R
#R.gPad
from rootpy.plotting import Canvas, Pad, Hist, Graph

from .toolbox import GraphStore, get_iy, get_ipt
from .config import load_cfg


__all__ =  [
        'RooParameters',
        ]

#Read config
cfg = load_cfg()

class RooParameters(object):
    """
    Read parameter text files from RooFit and create appropriate graphs
    """
    def __init__(self,filename):
        self.GraphStore = {}
        file_list = glob.glob(filename+'/*.txt')

        #Initialize Graphstore
        self.GraphStore = GraphStore(cfg['par_table'])

        for ifile in file_list:
            [iy, ipt, sys] = re.search(r"y(\d)_pt(\d+)_sys(\d+)",ifile).groups()
            iy = int(iy)
            ipt = int(ipt)
            sys = int(sys)

            with open(ifile) as ftmp:
                lines = [line.rstrip('\n') for line in ftmp]
                mean_pt = float(lines[0].split()[1])
                mean_y = float(lines[1].split()[1])
                nentries = float(lines[2].split()[1])
                #mean_y = (bins_y[iy]+bins_y[iy+1])/2

                for par_name in cfg['par_table']:
                    line_numbers = []

                    to_append = 0
                    sys_finished = False

                    for i, l in enumerate(lines):
                        if l.split()[0] == par_name:
                            to_append = i
                        if l.split()[0] == 'Chi2':
                            sys_finished = True
                            if par_name != 'Chi2' and not to_append:
                                #Some parameter is not free in the given systematics
                                to_append = -1

                        if sys_finished:
                            line_numbers.append(to_append)
                            sys_finished = False
                            to_append = 0

                    for line_no in line_numbers:
                        if line_no == -1:
                            self.GraphStore.fill(par_name, ipt, iy, mean_pt, mean_y, 0, [0,0], sys)
                            continue

                        data = lines[line_no].split()

                        #print(ifile,mean_pt, mean_y,data)
                        par_value = float(data[2])
                        if(data[3] == "+/-"):
                            if(data[4][0] == "("):
                                par_error = [float(data[4][2:-1]), float(data[5][:-1])]
                            else:
                                if float(data[4]) > 0.3*par_value:
                                    par_error = [float(data[4]), float(data[4])]
                                    #par_error = [0.3*par_value, 0.3*par_value]
                                else:
                                    par_error = [float(data[4]), float(data[4])]
                        elif data[3] == "C":
                            par_error = [0, 0]
                        else:
                            par_error = [999, 999]

                        self.GraphStore.fill(par_name, ipt, iy, mean_pt, mean_y, par_value, par_error, sys)
        #self.GraphStore.remove_empty_points()


    def __getitem__(self, item):
        """
        For given parameter name return a corresponding list of the graphs
        """
        return self.GraphStore.store[item]

    def __getattr__(self, name):
        return self[name]
