import numpy
import rootpy.ROOT as R
from rootpy.io import root_open
from rootpy.plotting import Canvas, Graph, Hist2D
from rootpy import log

from .config import load_cfg

__all__ = [
        'prepare_acc',
        'generate_acc',
        ]

#Read config
cfg = load_cfg()

def prepare_acc(ntuples, year, graphs_sigma):
    """
    Prepare maps for J/psi and psi'
    """
    acc_1S = root_open(cfg['acc_file_1S'])
    acc_2S = root_open(cfg['acc_file_2S'])

    log.info("Performing mean acceptance weights calculation of J/psi...")
    acc_map_1S = generate_acc(acc_1S.flat, ntuples, year, graphs_sigma)
    log.info("Performing mean acceptance weights calculation of psi(2S)...")
    acc_map_2S = generate_acc(acc_2S.flat, ntuples, year, graphs_sigma)

    c = Canvas(name="Acc_1S_2D", width=800, height=600)
    c.set_right_margin(0.15)
    c.set_top_margin(0.1)
    c.draw()
    acc_map_1S.draw('colztext')
    c.save_as(cfg['path_weight_2D']+c.name+'.pdf')

    c = Canvas(name="Acc_2S_2D", width=800, height=600)
    c.set_right_margin(0.15)
    c.set_top_margin(0.1)
    c.draw()
    acc_map_2S.draw('colztext')
    c.save_as(cfg['path_weight_2D']+c.name+'.pdf')

    acc_map_1S.name = 'psi_1S'
    acc_map_2S.name = 'psi_2S'

    with root_open('{0}Acceptance_{1}.root'.format(cfg['path_rootfiles'], year),'w') as acc_file:
        acc_map_1S.write()
        acc_map_2S.write()

    acc_map_1S.SetDirectory(0)
    acc_map_2S.SetDirectory(0)

    return (acc_map_1S, acc_map_2S)

def generate_acc(acc_map, ntuples, year, graphs_sigma):
    R.TH1.SetDefaultSumw2(True)
    out_map = Hist2D(len(cfg['bins_y'])-1, 0, len(cfg['bins_y'])-1, len(cfg['bins_pt'])-1, 0, len(cfg['bins_pt'])-1)
    out_map.yaxis.title = "#it{p}_{T} bin"
    out_map.xaxis.title = "|y| bin"
    out_map.xaxis.set_ndivisions(len(cfg['bins_y'])-1)
    #out_map.yaxis.set_ndivisions(len(cfg['bins_pt'])-1)
    out_map.zaxis.title = 'Average Acceptance'
    for iy, (y_lo, y_hi) in enumerate(zip(cfg['bins_y'], cfg['bins_y'][1:])):
        print(graphs_sigma[iy])
        for ipt, (pt_lo, pt_hi) in enumerate(zip(cfg['bins_pt'], cfg['bins_pt'][1:])):
            if pt_lo < 52.5: #Acceptance is for mu4mu52p5
                continue

            w_acc = []

            for i, ntuple in enumerate(ntuples):
                with root_open(ntuple) as f:
                    tree = f['tuple_y{0}_pt{1}'.format(iy, ipt)]
                    for e in tree:
                        if not (e.passTrigger & cfg['trigger_cut'][year][i]):
                            continue
                        if e.weight_reco >= 20 or not e.weight_trig_single:
                            continue
                        if e.pt1<52.5 and e.pt2<52.5:
                            continue
                        if e.m > 3.4 or e.m < 2.8:
                            continue
                        bin_no = acc_map.find_bin(abs(e.y), e.pt)
                        w_acc.append(1/acc_map[bin_no].value)

            out_map[iy+1, ipt+1].value = numpy.mean(w_acc)
    R.TH1.SetDefaultSumw2(False)
    return out_map
