from __future__ import absolute_import

import re
from rootpy.plotting import Graph, Hist2D
from rootpy import log
import matplotlib.pyplot as plt

from .config import load_cfg

__all__ = [
        'GraphStore',
        'MapStore',
        'get_iy',
        'get_ipt',
        'pt2y',
        'GraphStore2MapStore'
        'get_colors',
        'scientific',
        ]

#Read config
cfg = load_cfg()

y_re = re.compile("_y(\d+)")
pt_re = re.compile("_pt(\d+)")

class GraphStore(object):
    def __init__(self, parameters):
        self.store = {}
        self.sys_set = set()
        for par in parameters:
            self.store[par] = []

    def clone(self):
        """
        Return the clone of a given GraphStore
        """
        clone = GraphStore(self.store.keys())
        for par in self.store:
            for graph in self.store[par]:
                clone.insert(graph, True)

        return clone

    def insert(self, graph, clone=False):
        """
        Insert new graph in the GraphStore, graph name should follow to the
        naming style shown in insert_empty function
        The graph in store with same name as 'graph' is overwritten, if it
        exists
        """
        prj_name, i, par = re.search(r"graph_(y|pt)(\d+)_(.+)", graph.name).groups()

        if par not in self.store:
            self.store[par] = []
        else:
            #Delete graph with the same name from store if it already exists
            for stored_graph in self.store[par]:
                if graph.name == stored_graph.name:
                    self.store[par].remove(stored_graph)

        if clone:
            self.store[par].append(graph.clone(graph.name))
        else:
            self.store[par].append(graph)

        if prj_name == 'y':
            self.store[par] = sorted(self.store[par], key = lambda x: get_iy(x.name))
        elif prj_name == 'pt':
            self.store[par] = sorted(self.store[par], key = lambda x: get_ipt(x.name))

    def insert_empty(self, name, sys=0):
        """
        Insert an empty graph in the GraphStore.
        name should have a following format {prj_name}{i}_{par}, where:
        prj_name - either 'y' for pt dependent, or 'pt' for y dependent
        i - bin index of y or pt
        par - parameter name

        Output graph name will have a style graph_{prj_name}{i}_{par}
        """
        prj_name, i, par = re.search(r"(y|pt)(\d+)_(.+)", name).groups()

        if prj_name == 'y':
            bins = cfg['bins_pt']
        elif prj_name == 'pt':
            bins = cfg['bins_y']

        graph = Graph(len(bins)-1, name='graph_{0}{1}_{2}'.format(prj_name, i, par), type='asymm')
        try:
            self.store[par].append(graph)
        except KeyError:
            self.store[par] = []
            self.store[par].append(graph)

        if prj_name == 'y':
            self.store[par] = sorted(self.store[par], key = lambda x: get_iy(x.name))
        elif prj_name == 'pt':
            self.store[par] = sorted(self.store[par], key = lambda x: get_ipt(x.name))

        if sys:
            self.sys_set.add(sys)

        return graph

    def __getitem__(self, item):
        """
        Return the specific graph from a GraphStore
        """
        prj_name, i, par = re.search(r"(y|pt)(\d+)_(.+)", item).groups()

        try:
            graph = next(igraph for igraph in self.store[par] if
                    igraph.name == 'graph_{0}{1}_{2}'.format(prj_name, i, par))
            return graph
        except StopIteration:
            raise ValueError('graph_{0}{1}_{2} not found in the GraphStore'.format(prj_name, i, par))

    def fill(self, par, ipt, iy, mean_pt, mean_y, par_value, par_error, sys=0):
        """
        Determine which graph to fill, if none is found, append an empty graph.
        Used for filling of the pt dependent graphs, y dependent graphs can be
        produced afterwards using utils.pt2y function

        The output will be sorted by the bin index
        """
        #pT dependence graphs
        try:
            if sys == 0:
                graph = self['y{0}_{1}'.format(iy, par)]
            else:
                graph = self['y{0}_{1}_sys{2}'.format(iy, par, sys)]
        except:
            if sys == 0:
                graph = self.insert_empty('y{0}_{1}'.format(iy, par))
            else:
                graph = self.insert_empty('y{0}_{1}_sys{2}'.format(iy, par, sys),sys)

        #Fill
        graph[ipt] = (mean_pt, par_value)
        graph[ipt].x.error_hi = cfg['bins_pt'][ipt+1]-mean_pt
        graph[ipt].x.error_low = -cfg['bins_pt'][ipt]+mean_pt
        graph[ipt].y.error_low = par_error[0]
        graph[ipt].y.error_hi = par_error[1]

    def remove_empty_points(self):
        for par in self.store:
            graphs = self.store[par]
            for graph in graphs:
                for point in graph:
                    if point.y.value:
                        break
                graph = graph.Crop(cfg['bins_pt'][point.idx_], cfg['bins_pt'][-1])

class MapStore(object):
    def __init__(self, parameters):
        self.store = {}
        for par_name in parameters:
            self.store[par_name] = []

    def insert(self, map_vals, map_errs):
        """
        Insert a new map in the MapStore
        """
        par_name = re.search(r"map_(.+)", map_vals.name).groups()[0]

        if par_name not in self.store:
            self.store[par_name] = []

        self.store[par_name].append(map_vals)
        self.store[par_name].append(map_errs)

def get_iy(name):
    """
    Return corresponding y bin number of pt dependent graphs
    """
    return int(re.search(y_re, name).groups()[0])

def get_ipt(name):
    """
    Return corresponding pt bin number of y dependent graphs
    """
    return int(re.search(pt_re, name).groups()[0])

def pt2y(graph_store):
    """
    Produce y dependent GraphStore from pt dependent GraphStore
    """
    log.info("Producing y dependent plots")

    y_store = GraphStore(graph_store.store)
    for par_name in graph_store.store:
        #We don't need y dependent for sys-graphs
        if '_sys' in par_name:
            continue
        graphs_pt = graph_store.store[par_name]
        for points in zip(*graphs_pt):
            for i, point in enumerate(points):
                if i == 0:
                    graph_y = y_store.insert_empty('pt{0}_{1}'.format(point.idx_, par_name))

                mean_y = (cfg['bins_y'][i]+cfg['bins_y'][i+1])/2

                graph_y[i].x.value = mean_y
                graph_y[i].x.error_hi = cfg['bins_y'][i+1]-mean_y
                graph_y[i].x.error_low = mean_y-cfg['bins_y'][i]

                graph_y[i].y.value = point.y.value
                graph_y[i].y.error_hi = point.y.error_hi
                graph_y[i].y.error_low = point.y.error_low
    return y_store

def GraphStore2MapStore(graph_store):
    """
    Takes GraphStore as an input and produces the corresponding MapStore
    """
    log.info("Obtaining maps from graphs")

    map_store = MapStore(graph_store.store)
    for par_name in graph_store.store:
        map_vals = Hist2D(len(cfg['bins_y'])-1, 0, len(cfg['bins_y'])-1, len(cfg['bins_pt'])-1, 0, len(cfg['bins_pt'])-1, name='map_'+par_name)
        map_vals.yaxis.title = "#it{p}_{T} bin"
        map_vals.xaxis.title = "|y| bin"
        map_vals.xaxis.set_ndivisions(len(cfg['bins_y'])-1)

        map_errs = Hist2D(len(cfg['bins_y'])-1, 0, len(cfg['bins_y'])-1, len(cfg['bins_pt'])-1, 0, len(cfg['bins_pt'])-1, name='mapErr_'+par_name)
        map_errs.yaxis.title = "#it{p}_{T} bin"
        map_errs.xaxis.title = "|y| bin"
        map_errs.xaxis.set_ndivisions(len(cfg['bins_y'])-1)

        graphs = graph_store.store[par_name]

        for graph in graphs:
            if 'graph_y' in graph.name:
                iy = get_iy(graph.name)
            elif 'graph_pt' in graph.name:
                ipt = get_ipt(graph.name)

            for point in graph:
                if 'graph_y' in graph.name:
                    ipt = point.idx_
                elif 'graph_pt' in graph.name:
                    iy = point.idx_

                map_vals[iy+1, ipt+1].value = point.y.value
                map_errs[iy+1, ipt+1].value = (point.y.error_hi+point.y.error_low)/2

        map_store.insert(map_vals, map_errs)

    return map_store

def get_colors(n, name='gist_rainbow'):
    """
    Return n visually distinct colors
    """
    return plt.cm.get_cmap(name, n)

def scientific(par):
    power = 0

    if par >= 1:
        while int(par/10):
            power += 1
            par /=10
    elif par != 0:
        while par<1:
            power -= 1
            par *= 10

    return (par, power)
