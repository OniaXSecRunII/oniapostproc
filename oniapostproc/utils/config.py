import sys
import os
import oyaml as yaml

def merge_two_cfgs(cfg1, cfg2):
    cfg = cfg1.copy()
    cfg.update(cfg2)
    return cfg

def load_cfg():
    p_dir = os.path.dirname(os.path.abspath(__file__))
    #Default config
    with open(os.path.join(p_dir, '../../config.yml')) as cfg_file:
        cfg = yaml.safe_load(cfg_file)

    #Override desired settings from user provided config file, if it exists
    u_cfg_file = os.path.join(sys.path[0], 'config.yml')
    if os.path.exists(u_cfg_file):
        with open(u_cfg_file) as cfg_file:
            u_cfg = yaml.safe_load(cfg_file)

        cfg = merge_two_cfgs(cfg, u_cfg)

    return cfg
