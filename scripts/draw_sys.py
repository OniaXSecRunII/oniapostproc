import rootpy.ROOT as R

from rootpy.io import root_open
from rootpy.plotting import Graph, Canvas, base
from rootpy.plotting.utils import draw

from oniapostproc.config import *

R.gROOT.LoadMacro("plotstyle/AtlasStyle.C")
R.gROOT.LoadMacro("plotstyle/AtlasUtils.C")
R.gROOT.LoadMacro("plotstyle/AtlasLabels.C")
R.SetAtlasStyle()
R.TGaxis.SetMaxDigits(3)
R.TH1.SetDefaultSumw2(False)

colors = ['black', 'red', 'blue']

f_sys = root_open('./Output/RootFiles/sys.root')

graphs_reco = []
graphs_trig = []
for i, g in enumerate([f_sys['g_reco_y{0}'.format(iy)] for iy in range(3)]):
    graph = Graph(len(bins_pt)-1)
    for p in g:
        mean_pt = (bins_pt[p.idx_+1]+bins_pt[p.idx_])/2
        graph[p.idx_].x.value = mean_pt
        graph[p.idx_].x.error_hi = bins_pt[p.idx_+1]-mean_pt
        graph[p.idx_].x.error_low = -bins_pt[p.idx_]+mean_pt
        graph[p.idx_].y.value = p.y.value
    graph = graph.Crop(60,300)
    graph.drawstyle='p'
    graph.color = colors[i]
    graphs_reco.append(graph)

for i, g in enumerate([f_sys['g_trigger_y{0}'.format(iy)] for iy in range(3)]):
    graph = Graph(len(bins_pt)-1)
    for p in g:
        mean_pt = (bins_pt[p.idx_+1]+bins_pt[p.idx_])/2
        graph[p.idx_].x.value = mean_pt
        graph[p.idx_].x.error_hi = bins_pt[p.idx_+1]-mean_pt
        graph[p.idx_].x.error_low = -bins_pt[p.idx_]+mean_pt
        graph[p.idx_].y.value = p.y.value
    graph = graph.Crop(60,300)
    graph.drawstyle='p'
    graph.color = colors[i]
    graphs_trig.append(graph)

plotables_reco = graphs_reco[:]
graph0 = plotables_reco[0].clone()
plotables_reco.insert(0,graph0)
graph0.drawstyle = ''
graph0.color = 'white'
graph0.xaxis.title = "#it{p}_{T}(#mu#mu) [GeV]"
graph0.yaxis.title = 'Fractional uncertainty'
graph0.yaxis.set_title_size(0.04)
graph0.yaxis.set_title_offset(1.55)
graph0.xaxis.set_more_log_labels(True)

plotables_trig = graphs_trig[:]
graph0 = plotables_trig[0].clone()
plotables_trig.insert(0,graph0)
graph0.drawstyle = ''
graph0.color = 'white'
graph0.xaxis.title = "#it{p}_{T}(#mu#mu) [GeV]"
graph0.yaxis.title = 'Fractional uncertainty'
graph0.yaxis.set_title_size(0.04)
graph0.yaxis.set_title_offset(1.55)
graph0.xaxis.set_more_log_labels(True)

c = Canvas(width=800, height=600)
c.name = '2017_reco_eff_sys'
draw(plotables_reco, pad=c, logx=True, xlimits=(59,301))

R.ATLASLabel(0.2,0.88,"Internal");
for iy, graph in enumerate(plotables_reco):
    color = base.convert_color(graph.linecolor, 'root')
    R.myMarkerLineText(0.25,0.80-0.05*iy,1.3,color,20,color,1,"{0:.2f} < |#it{{y}}| < {1:.2f}".format(
        bins_y[iy],bins_y[iy+1]), 0.04);

c.save_as(path_pt_graphs+c.name+'.pdf')

#Trigger
c = Canvas(width=800, height=600)
c.name = '2017_trig_eff_sys'
draw(plotables_trig, pad=c, logx=True, xlimits=(59,301), ypadding=(0.5,0.4))

R.ATLASLabel(0.2,0.88,"Internal");
for iy, graph in enumerate(plotables_trig):
    color = base.convert_color(graph.linecolor, 'root')
    R.myMarkerLineText(0.25,0.80-0.05*iy,1.3,color,20,color,1,"{0:.2f} < |#it{{y}}| < {1:.2f}".format(
        bins_y[iy],bins_y[iy+1]), 0.04);

c.save_as(path_pt_graphs+c.name+'.pdf')
