from rootpy.io import root_open
from oniapostproc import init_workdir
from oniapostproc import OniaData, OniaCombData, LatexBox, DrawBox

init_workdir()
#onia_2015  = OniaData(2015,'../RunAna/Output_old/data15/mu50/L1C0/','../RunAna/weighted15_mu50.root')
onia_2015  = OniaData(2015,'../RunAna/Output/data15/L1C1/','../RunAna/weighted15_OLD.root', '../RootFiles/ntuple_charm_data15.root')
onia_2015.produce_graphs_y()
onia_2015.produce_maps()

#DrawBox(onia_2015).draw_pt(sys=True)
#DrawBox(onia_2015).draw_fitsys()

latex_2015 = LatexBox(onia_2015)
#latex_2015.par_map_pt_y()
latex_2015.table_sys()
latex_2015.par_maps()
latex_2015.fit_pt_variation()
latex_2015.fit_pt_sys()
latex_2015.fitresults()

onia_2016  = OniaData(2016,'../RunAna/Output/data16/L1C1/','../RunAna/weighted16_mu50.root', '../RootFiles/ntuple_charm_data16.root')
onia_2016.produce_graphs_y()
onia_2016.produce_maps()

#DrawBox(onia_2016).draw_fitsys()
latex_2016 = LatexBox(onia_2016)
#latex_2016.par_map_pt_y()
latex_2016.table_sys()
latex_2016.par_maps()
latex_2016.fit_pt_variation()
latex_2016.fit_pt_sys()
latex_2016.fitresults()

onia_2017  = OniaData(2017,'../RunAna/Output/data17/L1C1/','../RunAna/weighted17_mu50.root', '../RootFiles/ntuple_charm_data17.root')
onia_2017.produce_graphs_y()
onia_2017.produce_maps()

#DrawBox(onia_2017).draw_fitsys()
latex_2017 = LatexBox(onia_2017)
#latex_2017.par_map_pt_y()
latex_2017.table_sys()
latex_2017.par_maps()
latex_2017.fit_pt_variation()
latex_2017.fit_pt_sys()
latex_2017.fitresults()

onia_2018  = OniaData(2018,'../RunAna/Output/data18/L1C1/','../RunAna/weighted18_mu50.root', '../RootFiles/ntuple_charm_data18.root')
onia_2018.produce_graphs_y()
onia_2018.produce_maps()

latex_2018 = LatexBox(onia_2018)
#latex_2018.par_map_pt_y()
latex_2018.table_sys()
latex_2018.par_maps()
latex_2018.fit_pt_variation()
latex_2018.fit_pt_sys()
latex_2018.fitresults()

on_15_16_17_18 = onia_2015+onia_2016+onia_2017+onia_2018
on_15_16_17_18.par_vs_year()
on_15_16_17_18.produce_graphs_pt()

#graphs = on_15_16_17_18.par_graph
#DrawBox(on_15_16_17_18).draw_year()
print(on_15_16_17_18.graphs_pt.store)
on_15_16_17_18.produce_graphs_y()
on_15_16_17_18.produce_maps()


latex = LatexBox(on_15_16_17_18)
latex.par_map_pt_y()
latex.year_graph()

#graphs = on_15_16_17_18.par_graph
#with root_open('test.root','w') as f:
    #for key in graphs:
        #graphs[key].write()
